package com.login.bean;

public class Usuario {


	private String usuario;
	private String clave;

	private String email;

	

	public Usuario(String usuario, String clave) {

		this.usuario = usuario;
		this.clave = clave;

	}

	public Usuario(String usuario, String clave, String email) {
		this("","");
	}
	
	public Usuario() {
		
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
}
	
	
	
	
	
	
	

