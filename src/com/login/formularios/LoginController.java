package com.login.formularios;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import com.elementos.Obstaculo;
import com.escenas.Aplicacion;
import com.escenas.StageRPGConstructor;
import com.login.bean.Usuario;
import com.login.mantenimiento.GestionUsuario;
import com.rpg.ManejadorDB;
import com.rpg.constructor.view.CreadorObjetos;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class LoginController {
	
	
	
	@FXML
	private Stage subVentana;
	
	@FXML
	private ImageView logo;
	
	@FXML
	private TextField textfieldUsuario;
	
	@FXML
	private PasswordField fieldPassword;
	
	@FXML
	private Button botonSalir;
	
	@FXML
	private Button botonIngresar;
	
	@FXML
	private Hyperlink registrate;
	
	private Stage ventana;

	private String[] args;

	private StageRPGConstructor primaryStage;
	
	public static LoginController newInstance() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Aplicacion.class.getResource("../login/formularios/Login.fxml"));
		loader.load();

		LoginController nuevo = loader.getController();

		return nuevo;
	}
	
	@FXML
	protected void initialize() {
		
	}
	@FXML
	public void funcionBotonSalir() {
		 salir();
	}
	@FXML
	public void funcionBotonIngresar() {
		ingresar();
	}
	@FXML
	public void ingresar() {
		String usuario = textfieldUsuario.getText();
		String clave = DigestUtils.md5Hex((fieldPassword.getText()));
		
		GestionUsuario gestionUsuario = new GestionUsuario();
		
		Usuario usuario2 = new Usuario(usuario,clave);
		
		Usuario usu = gestionUsuario.obtenerUsuario(usuario2);

		if (usu != null) {
			JOptionPane.showMessageDialog(null, "Bienvenido");
			
			new Aplicacion().start(ventana);;
			subVentana.close();
		} else {
			JOptionPane.showMessageDialog(null, "Datos invalidos", "Error", JOptionPane.ERROR_MESSAGE);
			
		}

	}
	@FXML
	public void salir() {
		subVentana.close();
	}
	@FXML
	public void registrate() {
		
			try {
				new RegistroMain().start(ventana);
				subVentana.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
	}
	
	public void setVentana(Stage primaryStage) {
		this.ventana = primaryStage;
	subVentana.initOwner(primaryStage);
	subVentana.setOnCloseRequest(e -> System.exit(0));
	subVentana.setResizable(false);
	subVentana.show();

	}
	
}
	


