package com.login.formularios;

import java.io.IOException;
import java.net.URL;

import com.escenas.EscenaPrueba;
import com.escenas.StageRPGConstructor;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class LoginMain extends Application {
	
	public static void main(String[] args) {
		launch(args);
	} 
	

	@Override
    public void start(Stage primaryStage) throws IOException {
		LoginController.newInstance().setVentana(primaryStage);
	
	}
	
	
	
}	
	