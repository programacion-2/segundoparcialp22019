package com.login.formularios;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.apache.commons.codec.digest.DigestUtils;

import com.db.TablaUsuarios;
import com.escenas.Aplicacion;
import com.escenas.StageRPGConstructor;
import com.login.bean.Usuario;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RegistroController {

	@FXML
	private Stage subVentana;

	@FXML
	private TextField textfieldUsuario;

	@FXML
	private PasswordField textfieldContrasenia;

	@FXML
	private PasswordField textfieldRepetirContrasenia;

	@FXML
	private TextField textfieldEmail;

	@FXML
	private Button registrarse;

	@FXML
	private Button atras;

	private Stage ventana;
	private String clave, verificacionClave;

	@FXML
	public static RegistroController newInstance() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Aplicacion.class.getResource("../login/formularios/Registro.fxml"));
		loader.load();

		RegistroController nuevo = loader.getController();

		return nuevo;
	}

	@FXML
	public void setVentana(Stage primaryStage) {
		this.ventana = primaryStage;
		subVentana.initOwner(primaryStage);
		subVentana.setOnCloseRequest(e -> salir());
		subVentana.show();

	}

	@FXML
	protected void salir() {
		subVentana.close();
	}

	@FXML
	private void guardarDatos() {
		TablaUsuarios tabla = new TablaUsuarios();
		Usuario usuario = new Usuario();

		if (textfieldUsuario.getText().length()!=0 ) {
			usuario.setUsuario(textfieldUsuario.getText());}
		else {JOptionPane.showMessageDialog(null, "Error", "El campo Usuario esta vacio", JOptionPane.ERROR_MESSAGE);}
		if (textfieldEmail.getText().length()!=0) {
			usuario.setEmail(textfieldEmail.getText());}
		else {JOptionPane.showMessageDialog(null, "Error","El campo Email esta vacio", JOptionPane.ERROR_MESSAGE);}
		if (textfieldContrasenia.getText().length()!=0) {
			clave = DigestUtils.md5Hex(textfieldContrasenia.getText());}
		else {JOptionPane.showMessageDialog(null, "Error", "El campo Contrase�a esta vacio", JOptionPane.ERROR_MESSAGE);}
		if (textfieldRepetirContrasenia.getText().length()!=0) {
			verificacionClave = DigestUtils.md5Hex(textfieldRepetirContrasenia.getText());}
		else {JOptionPane.showMessageDialog(null, "Error", "El campo Repetir Contrase�a esta vacio", JOptionPane.ERROR_MESSAGE);}
		if (clave.equals(verificacionClave)) {
			usuario.setClave(clave);
			if(textfieldUsuario.getText().length() != 0 && textfieldEmail.getText().length() != 0 && textfieldContrasenia.getText().length() != 0 && textfieldRepetirContrasenia.getText().length() != 0 )
				try {
					tabla.agregarRegistro(usuario);
					JOptionPane.showMessageDialog(null, "Se ah registrado correctamente");
					subVentana.close();
					try {
						new LoginMain().start(ventana);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					} catch (SQLException e) {
	
						e.printStackTrace();
						}
				}

		else{JOptionPane.showMessageDialog(null, "Error", "Las contrase�as no coinciden", JOptionPane.ERROR_MESSAGE);}
}
	@FXML
	protected void getFuncionBotonAtras() {
		try {
			new LoginMain().start(ventana);
			subVentana.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@FXML
	protected void getFuncionBotonRegistrarse() {
		guardarDatos();
		subVentana.close();
	}

}
