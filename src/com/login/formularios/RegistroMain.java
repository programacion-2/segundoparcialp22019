package com.login.formularios;



import org.apache.commons.codec.digest.DigestUtils;

import javafx.application.Application;
import javafx.stage.Stage;




public class RegistroMain extends Application{
	public static void main(String[] args) {
		
		launch(args);
	} 
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		RegistroController.newInstance().setVentana(primaryStage);
		
	}
	
}