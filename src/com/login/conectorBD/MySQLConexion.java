package com.login.conectorBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConexion {

	public static Connection getConexion() {

		Connection con = null;

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");

			String usuario = "cesit";	
			String pass = "cesit";
			
			String hostname = "localhost";
			
			int port = 3306;
			
			
			String database="RPG_Test1";
			
			String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false&serverTimezone=UTC";
			
			
			
			con = DriverManager.getConnection(url, usuario, pass);

		} catch (ClassNotFoundException e) {
			System.out.println("ERROR ---> AL CARGAR EL DRIVER");

			e.printStackTrace();

		} catch (SQLException e) {
			System.out.println("ERROR ---> CON LA BASE DE DATOS");

			e.printStackTrace();
		}

		return con;
	}
}
