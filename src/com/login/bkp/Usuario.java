package com.login.bkp;

public class Usuario {

	private String name;
	private String pass;

	public Usuario(String name) throws InvalidNameException {
		super();
		setName(name);
		pass="";
	}

	public String getName() {
		return name;
	}

	private void setName(String name) throws InvalidNameException {
		if (name.contains(" "))
			throw new InvalidNameException("Nombre de usuario NO puede contener espacios");
		if (!name.contains("@"))
			throw new InvalidNameException("Nombre de usuario DEBE contener @");
		if (name.length()<4)
			throw new InvalidNameException("Nombre de usuario DEBE tener al menos 4 caracteres");
		this.name = name;
	}

	public boolean checkPass(String pass) {
		if (this.pass == pass)
			return true;
		return false;
	}

	public void setPass(String oldpass, String newpass) throws InvalidPassException {
		if (newpass.length()>8 || newpass.length()<4)
			throw new InvalidPassException();
		if (checkPass(oldpass))
			this.pass = newpass;
	}

}
