package com.login.bkp;

public class InvalidPassException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9043337599818974399L;

	public InvalidPassException() {
		super("Contraseña debe tener entre 4 y 8 caracteres");
		// TODO Auto-generated constructor stub
	}

}
