package com.login.bkp;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UsuarioTest {

	@Test
	void testUsuario() {
		testNombreInvalido("usuario", "Nombre invalido aceptado (Sin @)");
		testNombreInvalido("usuario con @", "Nombre invalido aceptado (con ' ')");
		testNombreInvalido("@u1", "Nombre invalido aceptado (muy corto)");
		
		try {
			Usuario test = new Usuario("user@prog");
			System.out.println(test.getName());
			System.out.println("nombre aceptado");
		} catch (InvalidNameException e) {
			System.err.println(e.getMessage());
			fail("Nombre valido no aceptado");

		}

	}

	void testNombreInvalido(String nombre, String fail) {
		try {
			System.out.println(nombre);
			@SuppressWarnings("unused")
			Usuario test = new Usuario(nombre);
			fail(fail);
		} catch (InvalidNameException e) {
			System.out.println(e.getMessage());
		}
	}

}
