package com.control;

import com.elementos.Suelo;
import com.escenas.StageRPGConstructor;
import com.tool.Coords;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

public class Mouse {

	private Coords lastRClick;
	private MouseEvent mousePos;
	private StageRPGConstructor stageMain;

	public Mouse(StageRPGConstructor stageMain) {
		this.stageMain = stageMain;
	}

	public Coords getMouseSueloPos() {
		Coords pos = getMousePos();
		pos.addX(-stageMain.getMundo().getSuelo().getPos().getKey() * Suelo.width);
		pos.addY(-stageMain.getMundo().getSuelo().getPos().getValue() * Suelo.heigth);
		return pos;
	}

	public Coords getMousePos() {
		return new Coords(
				mousePos.getX() / stageMain.getPantallaJuego().getTransformResolucionW()
						+ stageMain.getPantallaJuego().getCamX(),
				mousePos.getY() / stageMain.getPantallaJuego().getTransformResolucionH()
						+ stageMain.getPantallaJuego().getCamY());

	}

	public EventHandler<? super MouseEvent> getOnMouseMoved() {
		return e -> {
			this.mousePos = e;
		};
	}

	public EventHandler<? super MouseEvent> getOnMouseDragged() {

		return e -> {
			if (e.getButton() == MouseButton.PRIMARY)
				if (e.isControlDown()) {

					stageMain.getPantallaJuego().moveCam(
							(lastRClick.getX() - e.getX()) / stageMain.getPantallaJuego().getTransformResolucionW(),
							(lastRClick.getY() - e.getY()) / stageMain.getPantallaJuego().getTransformResolucionH());
//					hudGroup.setLayoutX(lastRClick.getX() - e.getX());
//					hudGroup.setLayoutY(lastRClick.getY() - e.getY());

					lastRClick = new Coords(e.getX(), e.getY());
					stageMain.setRecentrar(3.0);
				}

		};
	}

	public EventHandler<? super MouseEvent> OnMousePressed() {
		return e -> {
			lastRClick = new Coords(e.getX(), e.getY());
		};
	}

	public EventHandler<? super MouseEvent> getOnMouseClicked() {

		return e -> {
			if (e.getTarget().equals(stageMain.getControladorHud().getGeneralView()))
				if (e.getButton() == MouseButton.PRIMARY) {
					if (!e.isControlDown()) {
						Coords pos = getMousePos();
						if (!e.isAltDown())
							stageMain.getPersonaje().addDestino(pos);
						else {
							stageMain.getPersonaje().getRuta().clear();
							stageMain.getPersonaje().setDestino(pos);
						}
					}
				} else if (e.getButton() == MouseButton.SECONDARY)
					if (stageMain.isConstructorCerrado())
						stageMain.lanzarHabilidadSeleccionada();
					else
						stageMain.crearNuevoItem(getMouseSueloPos());
				else
					//TODO aca podemos poner evento con el boton central
					;
			else
				stageMain.getControladorHud().presionado(e.getTarget());
		};
	}

	public EventHandler<? super ScrollEvent> getOnScroll() {
		return e -> {

			int habilidad = stageMain.getControladorHud().getHabilidadSelect();
			if (e.getDeltaY() > 0)
				habilidad++;
			else if (e.getDeltaY() < 0)
				habilidad--;
			if (habilidad < 0)
				habilidad = 3;
			if (habilidad > 3)
				habilidad = 0;
			stageMain.getControladorHud().setHabilidadSelecionada(habilidad);

		};
	}
}
