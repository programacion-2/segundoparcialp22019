package com.control;

import com.elementos.Elemento;
import com.elementos.Personaje;
import com.elementos.proyectiles.Colision;
import com.elementos.proyectiles.Proyectil;
import com.escenas.StageRPGConstructor;
import com.rpg.constructor.ManejadorHabilidades;
import com.rpg.constructor.ManejadorHabilidades.LanzadorHabilidad;
import com.tool.Coords;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Teclado {

	private boolean pressW = false;
	private boolean pressA = false;
	private boolean pressS = false;
	private boolean pressD = false;

	public static boolean pressShift = false;
	public static boolean pressQ = false;

	private Runnable f2Funtion;
	private Runnable f3Funtion;
	private Runnable f4Funtion;

	private StageRPGConstructor stageMain;

	public Teclado(StageRPGConstructor stageMain) {
		this.stageMain = stageMain;
	}

	public void mover(Coords next) {
		if (pressW == true)
			next.addY(-10);
		if (pressS == true)
			next.addY(10);
		if (pressD == true)
			next.addX(10);
		if (pressA == true)
			next.addX(-10);

	}

	public boolean isMoviendo() {
		return (pressA == true) || (pressW == true) || (pressS == true) || (pressD == true);
	}

	public EventHandler<? super KeyEvent> getOnKeyPressed() {
		return e -> {
			if (e.getCode() == KeyCode.Q) {

				stageMain.lanzarHabilidadSeleccionada();
				pressQ = true;
			}
			if (e.getCode() == KeyCode.F2)
				if (f2Funtion != null)
					f2Funtion.run();
			if (e.getCode() == KeyCode.F3)
				if (f3Funtion != null)
					f3Funtion.run();
			if (e.getCode() == KeyCode.F4)
				if (f4Funtion != null)
					f4Funtion.run();

			if (e.getCode() == KeyCode.W) {
				pressW = true;
			}
			if (e.getCode() == KeyCode.S) {
				pressS = true;
			}
			if (e.getCode() == KeyCode.D) {
				pressD = true;
			}
			if (e.getCode() == KeyCode.A) {
				pressA = true;
			}
			if (e.getCode() == KeyCode.SHIFT) {
				pressShift = true;
			}
			if (e.getCode() == KeyCode.DIGIT1) {
				stageMain.getControladorHud().setHabilidadSelecionada(0);
			}
			if (e.getCode() == KeyCode.DIGIT2) {
				stageMain.getControladorHud().setHabilidadSelecionada(1);
			}
			if (e.getCode() == KeyCode.DIGIT3) {
				stageMain.getControladorHud().setHabilidadSelecionada(2);
			}
			if (e.getCode() == KeyCode.DIGIT4) {
				stageMain.getControladorHud().setHabilidadSelecionada(3);
			}

		}; 
	}

	public void setF2Funtion(Runnable f2Funtion) {

		this.f2Funtion = f2Funtion;
	}

	public void setF3Funtion(Runnable f3Funtion) {
		this.f3Funtion = f3Funtion;
	}
	
	public void setF4Funtion(Runnable f4Funtion) {
		this.f4Funtion = f4Funtion;
	}

	public EventHandler<? super KeyEvent> getOnKeyReleased() {
		return e -> {
			if (e.getCode() == KeyCode.W)
				pressW = false;
			if (e.getCode() == KeyCode.S)
				pressS = false;
			if (e.getCode() == KeyCode.D)
				pressD = false;
			if (e.getCode() == KeyCode.A)
				pressA = false;
			if (e.getCode() == KeyCode.SHIFT)
				pressShift = false;
			if (e.getCode() == KeyCode.Q)
				pressQ = false;
		};
	}

	public boolean isRunning() {
		return pressShift;
	}

}
