package com.elementos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map.Entry;

import com.escenas.TextSelector;
import com.pantalla.Image;
import com.pantalla.Pantalla;
import com.rpg.ManejadorDB;
import com.rpg.constructor.ManejadorNPC;
import com.rpg.constructor.ManejadorNPC.CreadorNPC;
import com.rpg.constructor.ManejadorObstaculos;
import com.tool.Coords;

import javafx.util.Pair;

public class Suelo {

	private List<Elemento> elementos;
	private List<Elemento>[][] hitBoxes;
	private Integer id;
	private int x, y;

	private Image fondo;
	boolean cargado = false;

	public static final int Xcuadrilla = 50;
	public static final int Ycuadrilla = 50;

	public static final int width = 1500;
	public static final int heigth = 1000;

	private Pair<Integer, Integer> pos;
	private Mundo mundo;
	private Pantalla pantalla;

	public Suelo(Integer id, Mundo mundo, Pair<Integer, Integer> pos) {

		elementos = new ArrayList<Elemento>();
		this.id = id;
		this.mundo = mundo;
		this.pos = pos;
		crearHitboxes();

		setFondo(new Image("file:recursos/imagenes/grass_large_hipass_small2.jpg", width, heigth));

	}

	private void crearHitboxes() {
		hitBoxes = new List[width / Xcuadrilla][heigth / Ycuadrilla];

		for (int i = 0; i < width / Xcuadrilla; i++)
			for (int j = 0; j < heigth / Ycuadrilla; j++) {
				hitBoxes[i][j] = new ArrayList<Elemento>();
			}
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	public void actualizar(double time) {
		cargar();
		pantalla.dibujar(this);
		getElementos().forEach(e -> mundo.actualizando().add(e));
	}

	private void cargar() {
		if (!cargado) {
			List<Obstaculo> o = ManejadorDB.instance().leerObstaculos(id);
			o.forEach(obstaculo -> obstaculo.colocar(this));
			ManejadorNPC.cargarSuelo(this);
			cargado = true;
		}

	}

	public void actualizarPeriferia(double time) {
		for (int x = pos.getKey() + 1; x >= pos.getKey() - 1; x--)
			for (int y = pos.getValue() + 1; y >= pos.getValue() - 1; y--) {
				Suelo actualizando = mundo.getSuelos().get(new Pair<Integer, Integer>(x, y));
				if (actualizando != null)
					actualizando.actualizar(time);
			}
	}

	public void colocar(Pantalla pantalla) {
		this.pantalla = pantalla;
	}

	public void addHitBox(Obstaculo nuevo) {
		double alto = nuevo.getSize().getY();
		double ancho = nuevo.getSize().getX();
		int x = (int) (nuevo.getX() - ancho / 2) / Xcuadrilla;
		int y = (int) (nuevo.getY() - alto / 2) / Ycuadrilla;
		for (int i = 0; i <= (int) ancho / Xcuadrilla; i++)
			for (int j = 0; j <= (int) alto / Ycuadrilla; j++) {
				List<Elemento> cercanos = getHitbox((x + i) * Xcuadrilla, (y + j) * Ycuadrilla);
				if (cercanos != null)
					cercanos.add(nuevo);
			}
	}

	public boolean getHitPoint(double x, double y) {
		return getHitPoint(x, y, false);
	}

	public boolean getHitPoint(double x, double y, boolean pickIntangible) {
		boolean ocupado = false;
		List<Elemento> cercanos = getHitbox(x, y);
		if (cercanos == null)
			return true;
		for (Elemento obstaculo : cercanos) {
			double i = 0, j = 0;
			if (!obstaculo.getSuelo().equals(this)) {
				i = -obstaculo.getSuelo().getX() + this.getX();
				j = -obstaculo.getSuelo().getY() + this.getY();
			}
			ocupado |= obstaculo.getHitPoint(x + i, y + j, pickIntangible);
		}
		return ocupado;
	}

	public List<Elemento> getHitbox(double x, double y) throws ArrayIndexOutOfBoundsException {
		if (x >= 0)
			if (x < width)
				if (y >= 0)
					if (y < heigth)
						return hitBoxes[(int) x / Xcuadrilla][(int) y / Ycuadrilla];
					else {
						if (getSur() != null)
							return getSur().getHitbox(x, y - heigth);
					}
				else {
					if (getNorte() != null)
						return getNorte().getHitbox(x, y + heigth);
				}
			else {
				if (getEste() != null)
					return getEste().getHitbox(x - width, y);
			}
		else {
			if (getOeste() != null)
				return getOeste().getHitbox(x + width, y);
		}
		return null;
	}

	public boolean getOcupado(double x, double y) {
		try {
			return !(getHitbox(x, y).isEmpty());
		} catch (ArrayIndexOutOfBoundsException e) {
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void soutPosString() {

		System.out.println("Suelo:"+id+"@"+pos+"/"+x+","+y);

	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Image getFondo() {
		return fondo;
	}

	public void setFondo(Image fondo) {
		this.fondo = fondo;
	}

	public Suelo getNorte() {
		return mundo.getSuelos().get(getLNorte());
	}

	public Pair<Integer, Integer> getLNorte() {

		Pair<Integer, Integer> lateral = new Pair<Integer, Integer>(pos.getKey(), pos.getValue() - 1);
		return lateral;
	}

	public void setNorte(int i) {
		mundo.nuevoSuelo(i, getLNorte());

	}

	public Suelo getSur() {
		return mundo.getSuelos().get(getLSur());
	}

	public Pair<Integer, Integer> getLSur() {

		Pair<Integer, Integer> lateral = new Pair<Integer, Integer>(pos.getKey(), pos.getValue() + 1);
		return lateral;
	}

	public void setSur(int i) {
		mundo.nuevoSuelo(i, getLSur());

	}

	public Suelo getEste() {
		return mundo.getSuelos().get(getLEste());
	}

	public Pair<Integer, Integer> getLEste() {

		Pair<Integer, Integer> lateral = new Pair<Integer, Integer>(pos.getKey() + 1, pos.getValue());
		return lateral;
	}

	public void setEste(int i) {

		mundo.nuevoSuelo(i, getLEste());

	}

	public Suelo getOeste() {
		return mundo.getSuelos().get(getLOeste());
	}

	public Pair<Integer, Integer> getLOeste() {

		Pair<Integer, Integer> lateral = new Pair<Integer, Integer>(pos.getKey() - 1, pos.getValue());
		return lateral;
	}

	public void setOeste(int i) {
		mundo.nuevoSuelo(i, getLOeste());

	}

	public Pair<Integer, Integer> getPos() {
		return pos;
	}

	public Pantalla getPantalla() {
		return pantalla;
	}

	public Mundo getMundo() {
		return mundo;
	}

}
