package com.elementos;

import com.tool.Coords;

import com.pantalla.Image;

public class Elemento {

	private Coords pos;
	protected Coords size;
	protected Image imagen;
	protected int ID;
	// private boolean desaparecer;
	protected Suelo suelo;
	private String nombre;
	private String url;

	public Elemento(String imagen, double x, double y, double w, double h) {
		this(x, y, w, h);
		setImagen(new Image(imagen, w, h));
		this.url = imagen;
		nombre = "";

	}

	public Elemento(double x, double y, double w, double h) {
		super();
		this.pos = new Coords(x, y);
		this.size = new Coords(w, h);
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Coords getPos() {
		return pos;
	}

	public void setPos(Coords pos) {
		this.pos = pos;
		if (suelo != null) {
			if (pos.getX() > Suelo.width + 1 && suelo.getEste() != null) {
				moverEste();
			} else if (pos.getY() > Suelo.heigth + 1 && suelo.getSur() != null) {
				moverSur();
			} else if (pos.getX() < 0 && suelo.getOeste() != null) {
				moverOeste();
			} else if (pos.getY() < 0 && suelo.getNorte() != null) {
				moverNorte();
			}
		}
		;
	}

	protected void moverNorte() {
//		desaparecer();
//		suelo = suelo.getNorte();
//		suelo.getElementos().add(this);
		pos.addY(Suelo.heigth);
		colocar(suelo.getNorte());
	}

	protected void moverOeste() {
//		desaparecer();
//		suelo = suelo.getOeste();
//		suelo.getElementos().add(this);
		pos.addX(Suelo.width);
		colocar(suelo.getOeste());
	}

	protected void moverSur() {
//		desaparecer();
//		suelo = suelo.getSur();
//		suelo.getElementos().add(this);
		pos.addY(-Suelo.heigth);
		colocar(suelo.getSur());
	}

	protected void moverEste() {
//		desaparecer();
//		suelo = suelo.getEste();
//		suelo.getElementos().add(this);
		pos.addX(-Suelo.width);
		colocar(suelo.getEste());
	}

	public void setPos(double x, double y) {
		pos.setX(x);
		pos.setY(y);
	}

	public double getX() {
//		if (suelo != null)
//			return pos.getX() + suelo.getX();
//		else
		return pos.getX();
	}

	public void setX(double x) {
//		if (suelo != null)
//			pos.setX(x - suelo.getX());
//		else
		pos.setX(x);
	}

	public double getY() {
//		if (suelo != null)
//			return pos.getY() + suelo.getY();
//		else
		return pos.getY();
	}

	public void setY(double y) {
//		if (suelo != null)
//			pos.setY(y - suelo.getY());
//		else
		pos.setY(y);
	}

	public double getGraficX() {
		return getSuelo().getX() + getX() - getSize().getX() / 2 - getSuelo().getPantalla().getCamX();
	}

	public double getGraficY() {
		return getSuelo().getY() + getY() - getSize().getY() / 2 - getSuelo().getPantalla().getCamY();

	}
	
	public double getAbsX() {
		return getSuelo().getX() + getX();
	}

	public double getAbsY() {
		return getSuelo().getY() + getY();

	}

	public void colocar(Suelo suelo){
		if (this.suelo!=null)
			this.suelo.getElementos().remove(this);
		this.suelo = suelo;
		this.setPos(this.getPos());
		if (!this.suelo.getElementos().contains(this))
		this.suelo.getElementos().add(this);
		
	}

	protected void dibujar() {
		suelo.getPantalla().dibujar(this);

	}

	public void actualizar(double time) {
		dibujar();
	}

	public Image getImagen() {
		return imagen;
	}

	public void setImagen(Image imagen) {
		this.imagen = imagen;
		if (getImagen().getFX() == null)
			imagen = null;
	}

	public Coords getSize() {
		return size;
	}

	public void setSize(Coords size) {
		this.size = size;
	}

	public Suelo getSuelo() {
		return suelo;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public double getIzq() {
		return getX() - size.getX() / 2;
	}

	public double getDer() {
		return getX() + size.getX() / 2;
	}

	public double getSup() {
		return getY() - size.getY() / 2;
	}

	public double getInf() {
		return getY() + size.getY() / 2;
	}

	public boolean getHitPoint(double x, double y, boolean pickIntangible) {
		return getHitPoint(x, y);
	}

	public boolean getHitPoint(double x, double y) {
		if (x > getIzq() && x < getDer() && y > getSup() && y < getInf())
			return true;
		return false;
	}

//	public boolean isDesaparecer() {
//		return desaparecer;
//	}
//
//	public void setDesaparecer(boolean desaparecer) {
//		this.desaparecer = desaparecer;
//	}

	public void desaparecer() {
		getSuelo().getElementos().remove(this);
	}
	@Override
	public String toString() {
		return super.toString()+"("+getNombre()+")";
	}

}
