package com.elementos;

import com.db.TablaObstaculos;
import com.rpg.constructor.ManejadorObstaculos;
import com.tool.Coords;

public class Obstaculo extends Elemento {

	public Obstaculo(String imagen, double x, double y, double w, double h) {
		super(imagen, x, y, w, h);
	}

	public Obstaculo(String imagen, double w, double h) {
		this(imagen, 0, 0, w, h);
	}

	private boolean intangible = false;

	public void setIntangible(boolean intangible) {
		this.intangible = intangible;
	}

	@Override
	public boolean getHitPoint(double x, double y) {
		if (intangible)
			return false;
		return super.getHitPoint(x, y);
	}

	@Override
	public boolean getHitPoint(double x, double y, boolean pickIntangible) {
		if (pickIntangible)
			return super.getHitPoint(x, y);
		return getHitPoint(x, y);
	}

	public boolean isTangible() {
		return !intangible;
	}

	@Override
	public void colocar(Suelo suelo) {
		suelo.addHitBox(this);
		super.colocar(suelo);
	}

	@Override
	protected void dibujar() {
		if (this.isTangible())
			suelo.getPantalla().dibujar(this);
	}
}
