package com.elementos.proyectiles;

import com.elementos.Personaje;
import com.tool.Coords;

public class Proyectil extends Personaje {

	private double direccion;
	private double desaceleracion;
	private Personaje lanzador;
	private Colision colision;

	public Proyectil(Personaje lanzador) {
		super();
		setSize(new Coords(5, 5));
		this.lanzador = lanzador;
		setPos(lanzador.getX(), lanzador.getY());
		setVelMax(5.0);
		setHp(3);
		desaceleracion = 0;
	}

	public double getDesaceleracion() {
		return desaceleracion;
	}

	public void setDesaceleracion(double desaceleracion) {
		this.desaceleracion = desaceleracion;
	}

	public double getDireccion() {
		return direccion;
	}

	public void setDireccion(double direccion, int distancia) {
		this.direccion = direccion;
		super.setDestino(getPos().calcularDireccion(direccion, distancia));

	}

	@Override
	public void actualizar(double time) {
		if (getVelMax() > 0 && getDestino() != null) {
			setVelMax(getVelMax() - desaceleracion / 10 * time);
			suelo.getElementos().stream().filter(e -> Personaje.class.isInstance(e)).forEach(p -> {
				if (p != lanzador && p != this)
					if (p.getHitPoint(getX(), getY()))
						colision.hit(p);
			});
		} else {
			setVelMax(0);
			super.setDestino(null);
			addHp(-time);
			if (getHp() < 0)
				desaparecer();
//				setDesaparecer(true);
		}

		super.actualizar(time);
	}

	public void setColision(Colision colision) {
		this.colision = colision;
	}
	

}
