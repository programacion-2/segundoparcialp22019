package com.elementos.proyectiles;

import com.elementos.Elemento;

public interface Colision {

	void hit(Elemento p);

}
