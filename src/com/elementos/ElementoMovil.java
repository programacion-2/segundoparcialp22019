package com.elementos;

import java.util.ArrayList;
import java.util.List;

import com.escenas.StageRPGConstructor;
import com.tool.Coords;

public abstract class ElementoMovil extends Elemento {

	protected double velMax;
	private Coords destino;
	private List<Coords> ruta;
	
	public ElementoMovil(double x, double y, double w, double h) {
		super(x, y, w, h);
		this.constructor();
	}

	public ElementoMovil(String imagen, double x, double y, double w, double h) {
		super(imagen, x, y, w, h);
		this.constructor();
	}

	private void constructor() {
		velMax = 1d;

		ruta = new ArrayList<Coords>();
	}

	public void addDestino(Coords destino) {

		if (this.destino == null)
			setDestino(destino);
		else
			ruta.add(destino);

	}



	public List<Coords> getRuta() {
		return ruta;
	}

	public Coords getDestino() {
		return destino;
	}

	public void setDestino(Coords destino) {
		if (getSuelo() != null && destino != null) {
			destino.addX(-getSuelo().getX());
			destino.addY(-getSuelo().getY());
		} else
			getRuta().clear();
		this.destino = destino;
	}

	public double getVelMax() {
		return velMax;
	}

	public void setVelMax(double velMax) {
		this.velMax = velMax;
	}



	public void mover(double time) {
		Coords next = calcularMovimiento(time);
		setPos(next);

	}

	public Coords calcularMovimiento(double time) {
		if (destino == null)
			return getPos();

		Coords next = getPos().calcularDireccion(calcularAngulo(destino), velMax * time*100);
		if (getPos().distancia(next) > getPos().distancia(destino)) {
			next=destino;
			if (!ruta.isEmpty()) {
				setDestino(ruta.get(0));
				ruta.remove(0);
			} else
				destino = null;}
		return next;

	}

	public double calcularAngulo(Coords destino) {
		return getPos().calcularAngulo(destino);
	}

	@Override
	protected void moverNorte() {
		super.moverNorte();
		if (destino != null)
			destino.addY(Suelo.heigth);
	}

	@Override
	protected void moverOeste() {
		super.moverOeste();
		if (destino != null)
			destino.addX(Suelo.width);
	}

	@Override
	protected void moverSur() {
		super.moverSur();
		if (destino != null)
			destino.addY(-Suelo.heigth);
	}

	@Override
	protected void moverEste() {

		super.moverEste();
		if (destino != null)
			destino.addX(-Suelo.width);

	}
	

}
