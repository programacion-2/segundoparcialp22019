package com.elementos;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.TablaMundoHasEscenarios;
import com.pantalla.Pantalla;
import com.rpg.ManejadorDB;

import javafx.util.Pair;

public class Mundo {

	private List<Elemento> elementos;

	private Map<Pair<Integer, Integer>, Suelo> suelos;
	Pair<Integer, Integer> suelo;
	private ManejadorDB db;
	private boolean cuadriculaVisible = false;
	private Pantalla pantalla;

	TablaMundoHasEscenarios dbEscenarios;

	private int id;

	private List<Elemento> elementosActualizando;

	public Mundo(Pantalla pantalla) {
		db = ManejadorDB.instance();
		this.pantalla = pantalla;

		suelos = new HashMap<Pair<Integer, Integer>, Suelo>();
		suelo = new Pair<Integer, Integer>(0, 0);

		elementosActualizando = new ArrayList<Elemento>();

		dbEscenarios = new TablaMundoHasEscenarios();
		dbEscenarios.addMundo(this);

		try {
			dbEscenarios.buscar("`id` = " + getId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// TODO esto tendria que salir de la DB escenarios
//		this.nuevoSuelo(1,suelo);
//		this.nuevoSuelo(2,new Pair<Integer, Integer>(1,0));
//		this.nuevoSuelo(3,new Pair<Integer, Integer>(0,-1));
//		this.nuevoSuelo(2,new Pair<Integer, Integer>(0,1));
//		this.nuevoSuelo(1,new Pair<Integer, Integer>(-1,0));
//		this.nuevoSuelo(1,new Pair<Integer, Integer>(1,-1));

//		getSuelo().setEste(2);
//		getSuelo().setNorte(3);
//		getSuelo().setSur(2);
//		getSuelo().setOeste(1);
//		getSuelo().getNorte().setEste(1);

		elementos = new ArrayList<Elemento>();
	}

	public Suelo nuevoSuelo(Integer i, Pair<Integer, Integer> suelo) {

		Suelo nuevo = new Suelo(i, this, suelo);
		suelos.put(suelo, nuevo);
		nuevo.setX(suelo.getKey() * Suelo.width);
		nuevo.setY(suelo.getValue() * Suelo.heigth);
		colocar(nuevo);
		return nuevo;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	private void colocar(Suelo suelo) {
		pantalla.agregar(suelo);
	}

	int i = 0;

	public void actualizar(double time) {

		getSuelo().actualizarPeriferia(time);

		try {
			actualizando().sort((e1, e2) -> {
				if (e1.getAbsY() > e2.getAbsY())
					return 1;
				if (e1.getAbsY() < e2.getAbsY())
					return -1;
				return 0;
			});
			actualizando().forEach(e -> e.actualizar(time));
		} catch (ConcurrentModificationException e) {

		}

		actualizando().clear();
		if (cuadriculaVisible) {
			pantalla.dibujarCuadricula(getSuelo());
		}

	}

	public Suelo getSuelo() {
		return suelos.get(suelo);
	}

//	public void setSuelo(Suelo suelo) {
//		suelos.put(this.suelo, suelo);
//	}

	public Map<Pair<Integer, Integer>, Suelo> getSuelos() {
		return suelos;
	}

	public void setSuelo(Pair<Integer, Integer> suelo) {
		this.suelo = suelo;
	}

	public void setSuelos(Map<Pair<Integer, Integer>, Suelo> suelos) {
		this.suelos = suelos;
	}

	public boolean isCuadriculaVisible() {
		return cuadriculaVisible;
	}

	public void setCuadriculaVisible(boolean cuadriculaVisible) {
		this.cuadriculaVisible = cuadriculaVisible;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Elemento> actualizando() {

		return elementosActualizando;
	}

}
