package com.elementos;

public interface Script {
	public void run(double time);
}
