package com.elementos;

import java.util.Optional;
import java.util.stream.Stream;

import com.elementos.proyectiles.Proyectil;
import com.tool.Coords;

import javafx.scene.paint.Color;

public class Npc extends Personaje {

	Script ia;
	private String key;

	public Npc() {
		super();
		setDefaultColor(Color.MAGENTA);
		setHp(100);
		setMaxHP(100);
		setVelMax(1.3);
	}

//	public Color getDefaultColor() {
//		return defaultColor;
//	}
//
//	public void setDefaultColor(Color defaultColor) {
//		this.defaultColor = defaultColor;
//	}

	protected void dibujar() {
		suelo.getPantalla().dibujar(this);
	}

	@Override
	public void actualizar(double time) {
//	if (getDestino() == null) 
//	  setDestino(new Coords(getX(), 20+getY()));
//	
//	
//	
//	
//	if (getDestino() != null)
//		  setDestino(new Coords(getX(), 20-getY())
//				);

//	if (getDestino() != null) {
//		setDestino(new Coords( getX(),2-getY()));
//		

//	}

		if (ia != null)
			ia.run(time);
		super.actualizar(time);
	}

	public void setIa(Script ia) {
		this.ia = ia;
	}

	public String getKey() {

		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	public void morir() {
		desaparecer();
	}


}
