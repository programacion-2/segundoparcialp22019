package com.elementos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.elementos.proyectiles.Proyectil;
import com.pantalla.Animacion;
import com.rpg.constructor.ManejadorHabilidades;
import com.tool.Coords;

import javafx.scene.paint.Color;

public class Personaje extends ElementoMovil {

	private Color defaultColor;
	private double hp = 100;
	private double maxHP = 100;
	private double recuperacionHp = 0;

	private double mana = 200;
	private double maxMana = 200;
	private double recuperacionMana = 10;

	public double resistencia = 100;
	private int recuperacion = 0;
	private final int RECUPERACION_MAXIMA = 100;
	private boolean corriendo = false;
	private double recuperacionStamina = 5;
	private double consumoStamina = 50;

	private String clan;
	private double ataque;

	private Coords checkpoint;
	private Suelo checksuelo;

	private List<ManejadorHabilidades.LanzadorHabilidad> habilidades;

	private Animacion movimiento;

	public Personaje() {
		super(0, 0, 80, 80);
		habilidades = new ArrayList<ManejadorHabilidades.LanzadorHabilidad>();
		defaultColor = Color.AQUAMARINE;
		setVelMax(3.8);
		clan = "";
	}

	public void setMovimiento(String carpeta, int cantidad) {

		movimiento = new Animacion(getSize().getX(), getSize().getY());
		movimiento.cargarImagenes(carpeta, cantidad);
	}

	public Animacion getMovimiento() {
		return movimiento;
	}

	public Color getDefaultColor() {
		return defaultColor;
	}

	public void setDefaultColor(Color defaultColor) {
		this.defaultColor = defaultColor;
	}

	@Override
	protected void dibujar() {
		suelo.getPantalla().dibujar(this);
	}

	@Override
	public void actualizar(double time) {

		Coords next = this.calcularMovimiento(time);
		if (!getSuelo().getHitPoint(next.getX(), next.getY()))
			this.setPos(next);
		else
			this.setDestino(null);

		this.recuperarMana(time);
		this.recuperarStamina(time);

		if (movimiento != null && getDestino() != null)
			movimiento.avanzar(time * velMax);

		this.chocar(time);
		super.actualizar(time);
	}

	private void chocar(double time) {
		getSuelo().getElementos().stream().filter(e -> Personaje.class.isInstance(e)).forEach(p -> {
//			if (p.getClass()==Personaje.class)
//				System.out.println("distancia con el personaje:"+getPos().distancia(p.getPos()));
			if (getPos().distancia(p.getPos()) < (getSize().getX() + p.getSize().getX()) / 2) {
				if (!clan.equals(((Personaje) p).getClan()))
					((Personaje) p).addHp(-ataque * time);
			}
		});

	}

	public double getHp() {
		return hp;
	}

	public void setHp(double hp) {
		this.hp = hp;
	}

	public double addHp(double hp) {
		this.hp += hp;
		return this.hp;
	}

	public double getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(double maxHP) {
		this.maxHP = maxHP;
	}

	public double getMana() {
		return mana;
	}

	public void setMana(double mana) {
		this.mana = mana;
	}

	public double addMana(double mana) {
		this.mana += mana;
		return this.mana;
	}

	public double getMaxMana() {
		return maxMana;
	}

	public void setMaxMana(double maxMana) {
		this.maxMana = maxMana;
	}

	public void recuperarMana(double time) {
		if (this.mana < maxMana) {

			if (this.mana >= maxMana * 0.75) {
				this.mana += 0.5 * time * recuperacionMana;
			} else if (this.mana >= maxMana * 0.5) {
				this.mana += 1 * time * recuperacionMana;
			} else {
				this.mana += 2 * time * recuperacionMana;
			}
		}
		
		if (this.hp < maxHP) {

			if (this.hp >= maxHP * 0.75) {
				this.hp += 0.5 * time * recuperacionHp;
			} else if (this.hp >= maxHP * 0.5) {
				this.hp += 1 * time * recuperacionHp;
			} else {
				this.hp += 2 * time * recuperacionHp;
			}
		}
	}

	public double getAtaque() {
		return ataque;
	}

	public void setAtaque(double ataque) {
		this.ataque = ataque;
	}

	public double getRecuperacionMana() {
		return recuperacionMana;
	}

	public void setRecuperacionMana(double recuperacionMana) {
		this.recuperacionMana = recuperacionMana;
	}

	public String getClan() {
		return clan;
	}

	public void setClan(String clan) {
		this.clan = clan;
	}

	public void setCheckpoint(Coords checkpoint, Suelo checksuelo) {
		this.checkpoint = checkpoint;
		this.checksuelo = checksuelo;
	}

	public void morir() {
		setHp(maxHP);
		setMana(maxMana);
		setPos(checkpoint);
		colocar(checksuelo);
	}

	public double getResistencia() {
		return resistencia;
	}

	public void setResistencia(double resistencia) {
		this.resistencia = resistencia;
	}

	public int getRECUPERACION_MAXIMA() {
		return RECUPERACION_MAXIMA;
	}

	public void correr() {
		if (resistencia > 0) {
			this.velMax = 3.9;
			corriendo = true;
		} else {
			this.velMax = 1.9;
			corriendo = false;
		}
	}

	private void recuperarStamina(double time) {
		if (resistencia < -5)
			resistencia = -5;
		if (corriendo)
			resistencia -= consumoStamina * time;
		else if (resistencia < RECUPERACION_MAXIMA) {
			resistencia += recuperacionStamina * time;
		}
//		if (recuperacion == RECUPERACION_MAXIMA && resistencia < 50) {
//			resistencia+=recuperacionStamina*time;
//
//		}

	}

	public void caminar() {
		this.velMax = 1.9;
		corriendo = false;
	}

	public ManejadorHabilidades.LanzadorHabilidad getHabilidad(int i) {
		try {
			return habilidades.get(i);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public void addHabilidad(ManejadorHabilidades.LanzadorHabilidad habilidad) {
		if (habilidad != null)
			if (!this.habilidades.contains(habilidad))
			this.habilidades.add(habilidad);
	}

	public Stream<Elemento> buscarAliados() {
		return buscarPersonajes().filter(e -> ((Personaje) e).getClan().equals(this.getClan()));
	}

	public Stream<Elemento> buscarEnemigos() {
		return buscarPersonajes().filter(e -> !((Personaje) e).getClan().equals(this.getClan()));
	}

	public Stream<Elemento> buscarPersonajes() {
		return this.getSuelo().getElementos().stream().filter(e -> {
			if (!Personaje.class.isInstance(e) || Proyectil.class.isInstance(e))
				return false;
			return !((Personaje) e).getClan().equals("Escenario");
		});
	}

	public void buff() {
		recuperacionStamina*=2;
		consumoStamina/=2;
		maxMana*=2;
		recuperacionMana*=2;
		maxHP*=2;
		recuperacionHp = 10;
		
	}

}
