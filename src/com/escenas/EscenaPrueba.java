package com.escenas;

import javafx.scene.Group;

public class EscenaPrueba extends Escena {

	public EscenaPrueba(Group root) {
		super(root);
		CanvasPrueba canvas = new CanvasPrueba();
		root.getChildren().add(canvas);
		setOnKeyPressed(canvas.getHandle());
	}
}
