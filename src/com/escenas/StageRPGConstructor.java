package com.escenas;

import java.io.IOException;
import java.util.List;

import com.control.Mouse;
import com.control.Teclado;
import com.db.TablaObstaculos;
import com.elementos.Mundo;
import com.elementos.Npc;
import com.elementos.Obstaculo;
import com.elementos.Personaje;
import com.elementos.Suelo;
import com.login.formularios.LoginController;
import com.pantalla.Pantalla;
import com.pantalla.PantallaFX;
import com.rpg.ManejadorDB;
import com.rpg.hudController;
import com.rpg.constructor.ManejadorHabilidades;
import com.rpg.constructor.ManejadorNPC;
import com.rpg.constructor.ManejadorObstaculos;
import com.rpg.constructor.ManejadorHabilidades.LanzadorHabilidad;
import com.rpg.constructor.view.CreadorObjetos;
import com.tool.Coords;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class StageRPGConstructor extends Stage {

	private static StageRPGConstructor _lastInstance;
	private Mundo mundo;
	private PantallaFX pantallaJuego;

	private Teclado teclado;

	private CreadorObjetos creadorObstaculo;
	private LoginController login;
	private Mouse mouse;

	private hudController controladorHud;

	private ManejadorNPC manejadorNPC;
	private Personaje personaje;
	private Group root;
	protected double recentrar = 0;

	public StageRPGConstructor() {
		super();
		_lastInstance = this;

		mouse = new Mouse(this);
		teclado = new Teclado(this);

		crearPantalla();
		crearHud();

		crearPersonaje();
		manejadorNPC = new ManejadorNPC();

		crearAnimation();

		crearMouseEvent();
		crearTecladoEvent();
//		Npc npc = manejadorNPC.nuevo("Deambulante").crear(300, 300);
//		npc.colocar(mundo.getSuelo());
//		npc.setClan("Jugador");
//		npc.setHp(1000000000);
//		npc.setMaxHP(1000000000);
	}

	private void crearTecladoEvent() {
		teclado.setF2Funtion(() -> mostrarConstructor());
		teclado.setF3Funtion(() -> mundo.setCuadriculaVisible(!mundo.isCuadriculaVisible()));
		teclado.setF4Funtion(() -> mostrarLogin());

		root.getScene().setOnKeyPressed(teclado.getOnKeyPressed());
		root.getScene().setOnKeyReleased(teclado.getOnKeyReleased());
	}

	private void crearMouseEvent() {
		root.getScene().setOnMouseClicked(mouse.getOnMouseClicked());
		root.getScene().setOnMousePressed(mouse.OnMousePressed());
		root.getScene().setOnMouseMoved(mouse.getOnMouseMoved());
		root.getScene().setOnMouseDragged(mouse.getOnMouseDragged());
		root.getScene().setOnScroll(mouse.getOnScroll());
	}

	private void crearAnimation() {
		AnimationTimer loop = new AnimationTimer() {

			long then = 0;
			private int fps = 0;
			private int segundos;

			@Override
			public void handle(long now) {
				long time;
				if (then != 0)
					time = now - then;
				else
					time = 0;
				then = now;

				if (personaje.getHp() < 0)
					personaje.morir();
				controladorHud.setHp(personaje.getHp() / personaje.getMaxHP() * 100);
				controladorHud.setMana(personaje.getMana() / personaje.getMaxMana() * 100);
				controladorHud.setStamina(personaje.getResistencia() / personaje.getRECUPERACION_MAXIMA() * 100);

				pantallaJuego.limpiar();
				mundo.actualizar((double) time / (double) 1000000000);
				pantallaJuego.dibujarRuta(personaje);

				mundo.setSuelo(personaje.getSuelo().getPos());
				int segundos = (int) (now / 1000000000);
				if (segundos > this.segundos) {
					StageRPGConstructor.this.setTitle("Segundos: " + segundos + " FPS: " + fps);
					fps = 0;
					this.segundos = segundos;

				} else
					fps++;

				Coords next = new Coords(personaje.getAbsX(), personaje.getAbsY());

				teclado.mover(next);

				if (teclado.isMoviendo())
					personaje.setDestino(next);

				if (teclado.isRunning())
					personaje.correr();
				else
					personaje.caminar();
				if (recentrar < 0)
					centrarPersonajeEnPantalla();
				else
					recentrar -= ((double) time / (double) 1000000000);
			}

		};
		loop.start();
		setOnCloseRequest(e -> {
			loop.stop();
		});

	}

	protected void centrarPersonajeEnPantalla() {
		getPantallaJuego().moveCam(
				personaje.getGraficX()
						- getPantallaJuego().getSize().getX() / 2 / getPantallaJuego().getTransformResolucionW(),
				personaje.getGraficY()
						- getPantallaJuego().getSize().getY() / 2 / getPantallaJuego().getTransformResolucionH());
	}

	private void crearHud() {
		Group hudGroup = new Group();
		root.getChildren().add(hudGroup);

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Aplicacion.class.getResource("../rpg/hud.fxml"));

		try {
			Group hud = (Group) loader.load();
			hudGroup.getChildren().add(hud);

			controladorHud = loader.getController();
			controladorHud.setHp(100);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void crearPantalla() {
		root = new Group();
		setScene(new Scene(root, 1366, 768, Color.BLACK));
		show();

		setFullScreen(true);

		Group mundoGroup = new Group();
		root.getChildren().add(mundoGroup);
		pantallaJuego = new PantallaFX(root, 1366, 768);
		mundo = new Mundo(pantallaJuego);

	}

	private void crearPersonaje() {
		personaje = new Personaje();
		personaje.setPos(new Coords(100d, 100d));
		personaje.setDestino(new Coords(500d, 500d));
		personaje.setVelMax(1.9);
		personaje.colocar(mundo.getSuelo());
		personaje.setHp(100);
		personaje.setMaxHP(100);
		personaje.setClan("Jugador");
		personaje.setResistencia(100);
		personaje.setCheckpoint(new Coords(100d, 100d), personaje.getSuelo());
		personaje.addHabilidad(ManejadorHabilidades.instance().get("disparoBasico"));
		controladorHud.cargarHabilidades(personaje);
		personaje.setMovimiento("file:recursos/imagenMovimientoPersonaje", 4);
	}

	public void crearNuevoItem(Coords pos) {
		if (creadorObstaculo.getPaneObjetos().isExpanded()) {
			Obstaculo o = creadorObstaculo.getObstaculo();
			if (o != null)
				ManejadorObstaculos.instance().crearObstaculo(o, pos.getX(), pos.getY());
		} else if (creadorObstaculo.getPaneNPC().isExpanded()) {
			manejadorNPC.instance().crearNPC(creadorObstaculo.getSelectNPC(), pos.getX(), pos.getY());

		}

	}

	private void mostrarConstructor() {
		if (creadorObstaculo == null)
			try {
				creadorObstaculo = CreadorObjetos.newInstance();

				List<Obstaculo> obstaculos = ManejadorDB.instance().cargarObstaculosGenericos();
				obstaculos.forEach(obstaculo -> {
					creadorObstaculo.getSelectObscaculo().getLista().put(obstaculo.getNombre(), obstaculo);
				});

				creadorObstaculo.getSelectNPC().setLista(manejadorNPC.getCreadores());

				creadorObstaculo.setVentana(this);

			} catch (IOException e) {
				e.printStackTrace();
			}

	}

	public void mostrarLogin() {
		if (login == null)

			try {
				login = LoginController.newInstance();

				login.setVentana(this);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	public void cerrarConstructor() {
		this.creadorObstaculo = null;
	}

	public boolean isConstructorCerrado() {
		return (this.creadorObstaculo == null);
	}

	public Mouse getMouse() {
		return mouse;
	}

	public Personaje getPersonaje() {
		return personaje;
	}

	public Mundo getMundo() {
		return mundo;
	}

	public Pantalla getPantallaJuego() {
		return pantallaJuego;
	}

	public void setRecentrar(double recentrar) {
		this.recentrar = recentrar;
	}

	public int getHabilidadSelect() {

		return controladorHud.getHabilidadSelect();
	}

	public hudController getControladorHud() {
		return controladorHud;
	}

	public void lanzarHabilidadSeleccionada() {
		Personaje personaje = getPersonaje();
		try {
			LanzadorHabilidad habilidad = personaje.getHabilidad(getHabilidadSelect());
			if (habilidad != null)
				habilidad.lanzar(personaje, getMouse().getMouseSueloPos());
		} catch (IndexOutOfBoundsException excepcionSinImportancia) {
		}

	}

	public static StageRPGConstructor instance() {
		return _lastInstance;
	}

}
