package com.escenas;

import javax.swing.JOptionPane;

import com.game1.VentanaPingPong;
import com.game1.VentanaPingPongCliente;
import com.game1.VentanaPingPongServer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class EscenaPongMenu extends Scene {

	protected Group root;
	private Boton regresar;
	
	public EscenaPongMenu(Group root) {
		super(root,200d,400d,Color.AQUA);
		this.root = root;
		Boton game1 = new Boton("PONG", 50d, 40d, root);

		game1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				new VentanaPingPong().setVisible(true);

			}
		});

		Boton game2 = new Boton("Server",50d, 120d, root);
		
		game2.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				new VentanaPingPongServer().setVisible(true);
				
			}
		});
		Boton game3 = new Boton("Cliente",50d, 200d, root);
		
		game3.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				new VentanaPingPongCliente(JOptionPane.showInputDialog("IP del Servidor","localhost")).setVisible(true);
				
			}
		});

		regresar = new Boton("Volver", 50d, 280d, root);

		
	}

	public Boton getRegresar() {
		return regresar;
	}
	

}
