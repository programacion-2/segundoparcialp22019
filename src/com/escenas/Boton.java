package com.escenas;

import javafx.scene.Group;
import javafx.scene.control.Button;

public class Boton extends Button {
	private Double x, y;

	public Boton(String nombre, Double x, Double y, Group root) {

		this.x = x;
		this.y = y;
		this.setText(nombre);

		this.setDefaultButton(true);
		this.setPrefSize(100, 50);
		this.setLayoutX(x);
		this.setLayoutY(y);

		root.getChildren().add(this);
	}

}
