package com.escenas;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.game1.VentanaPingPong;
import com.game1.VentanaPingPongCliente;
import com.game1.VentanaPingPongServer;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Aplicacion extends Application {

	public static void main(String[] args) {

		launch(args);
	}

	private EventHandler<ActionEvent> regreso;
	private Stage primaryStage;
	private Scene menuPrincipal;
	private Group rootPrincipal;
	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage;
		rootPrincipal = new Group();
		
		menuPrincipal= new Scene(rootPrincipal,200d,400d,Color.valueOf("#636eaa"));
		
		EscenaPongMenu pong= new EscenaPongMenu(new Group());
		EscenaRPGMenu rpg= new EscenaRPGMenu(new Group());
		
		primaryStage.setScene(menuPrincipal);
		primaryStage.setFullScreen(false);
		
		regreso = createBack();
		
		
		Boton game1 = new Boton("PONG", 50d, 40d, rootPrincipal);
		
		game1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {

				
				primaryStage.setScene(pong);
				pong.getRegresar().setOnAction(regreso);
				 

			}
		});
		
		Boton game2 = new Boton("RetroMito",50d, 120d, rootPrincipal);
		
		game2.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				primaryStage.setScene(rpg);
				rpg.getRegresar().setOnAction(regreso);
				
				
			}
		});
		
		
		
		
		
		Boton game4 = new Boton("TEST2", 50d, 280d, rootPrincipal);

		game4.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {

				Stage secundaryStage = new Stage();
				secundaryStage.setScene(new EscenaPrueba(new Group()));
				secundaryStage.show();

			}
		});
		
		
		

		primaryStage.show();
	}
	private EventHandler<ActionEvent> createBack() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				primaryStage.setScene(menuPrincipal);

			}
		};
		
	}

}
