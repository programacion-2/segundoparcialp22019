package com.escenas;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

public class EscenaRPGMenu extends Scene {

	protected Group root;
	private Boton regresar;

	public EscenaRPGMenu(Group root) {
		super(root, 200d, 400d, Color.AQUA);
		this.root = root;
		Boton game1 = new Boton("Iniciar", 50d, 40d, root);
		
		game1.setOnAction(e-> {
				new StageRPGConstructor();
		});

//		Boton game2 = new Boton("", 50d, 120d, root);
//
//		game2.setOnAction(new EventHandler<ActionEvent>() {
//
//			@Override
//			public void handle(ActionEvent arg0) {
//
//			}
//		});
//		Boton game3 = new Boton("", 50d, 200d, root);
//
//		game3.setOnAction(new EventHandler<ActionEvent>() {
//
//			@Override
//			public void handle(ActionEvent arg0) {
//
//			}
//		});
		regresar = new Boton("volver", 50d, 280d, root);

	}

	public Boton getRegresar() {
		return regresar;
	}

}
