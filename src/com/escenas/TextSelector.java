package com.escenas;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

public class TextSelector<T> extends TextField {

	private ContextMenu menu = new ContextMenu();
	private Map<String, T> lista = new HashMap<String, T>();

	public TextSelector() {
		super();
		this.setContextMenu(menu);
		menu.setOnAction(evento -> setText(((MenuItem) evento.getTarget()).getText()));
		this.setOnKeyTyped(e -> this.openLinst());
	}

	public void openLinst() {
		menu.getItems().clear();

		lista.keySet().stream().filter(item -> {

			if (item.toLowerCase().contains(getText().toLowerCase()))
				return true;
			return false;
		}).forEach(item -> menu.getItems().add(new MenuItem(item)));
		;
		menu.show(this, Side.BOTTOM, 0, 0);
	}

	public T getValue() {
		return lista.get(getText());
	}

	public TextSelector(String arg0) {
		this();
		setText(arg0);
	}

	public void setLista(Map<String, T> lista) {
		this.lista = lista;
	}

	public Map<String, T> getLista() {
		return lista;
	}

}
