package com.escenas;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class CanvasPrueba extends Canvas {
	long then = 0;
	double a = 50;
	double b = 56;

	public CanvasPrueba() {
		super(5000, 500);

		GraphicsContext lapiz = getGraphicsContext2D();

		new AnimationTimer() {

			@Override
			public void handle(long now) {
				// TODO Auto-generated method stub
				long time = now - then;
				if (then == 0) {
					time = 0;
				}
				then = now;
				a = a + (double) time / 100000000;

				lapiz.clearRect(0, 0, 5000, 500);

				lapiz.fillRect(a, b, 41, 22);
			}
		}.start();

	}

	public EventHandler getHandle(){
		return new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {

				if (event.getCode().equals(KeyCode.DOWN))
					b = b+ 40;
				if (event.getCode().equals(KeyCode.UP))
					b = b- 40;
				if (event.getCode().equals(KeyCode.RIGHT))
					a=a+40;
				if (event.getCode().equals(KeyCode.LEFT))
					a=a-40;
			}
		};
	}

}