package com.pantalla;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Animacion {

	private Map<String, List<Image>> imagenes;

	private double longitud = 2.0;
	private double width;
	private double heigth;

	private int cantidadFrame;

	private int frame = 0;
	private double siguiente = 0;

	public Animacion(double width, double heigth) {
		super();
		this.width = width;
		this.heigth = heigth;

		imagenes = new HashMap<String, List<Image>>();
		imagenes.put("Arriba", new ArrayList<Image>());
		imagenes.put("Abajo", new ArrayList<Image>());
		imagenes.put("Derecha", new ArrayList<Image>());
		imagenes.put("Izquierda", new ArrayList<Image>());
	}

	public Image getImage(Double direccion) {
		List<Image> secuencia;
		if (direccion == null) {
			secuencia = imagenes.get("Abajo");
			frame = 0;
		} else {
			secuencia = imagenes.get("Derecha");
			if (direccion < 0)
				direccion += 2 * Math.PI;
			if (direccion > Math.PI * 0.251)
				if (direccion <= Math.PI * 0.751)
					secuencia = imagenes.get("Abajo");
				else if (direccion <= Math.PI * 1.251)
					secuencia = imagenes.get("Izquierda");
				else if (direccion <= Math.PI * 1.751)
					secuencia = imagenes.get("Arriba");
		}

		return secuencia.get(frame);

	}

	public void avanzar(double distancia) {
		siguiente -= distancia;
		if (siguiente < 0) {
			frame++;
			siguiente += longitud / cantidadFrame;
		}
		if (frame >= cantidadFrame)
			frame = 0;
	}

	public void cargarImagenes(String carpeta, int cant) {
		cantidadFrame = cant;
		for (String direccion : imagenes.keySet())
			for (int i = 1; i <= cant; i++)
				imagenes.get(direccion).add(new Image(carpeta + "/" + direccion + i + ".png", width, heigth));
	}

}
