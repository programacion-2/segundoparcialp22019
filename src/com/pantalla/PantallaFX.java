package com.pantalla;

import com.elementos.Elemento;
import com.elementos.Mundo;
import com.elementos.Npc;
import com.elementos.Obstaculo;
import com.elementos.Personaje;
import com.elementos.Suelo;
import com.pantalla.Pantalla;
import com.tool.Coords;

import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.transform.Scale;

public class PantallaFX extends Pantalla {

	private Group root;

//	Map<Suelo, Canvas> canvas;

	Canvas hoja;

	private GraphicsContext lapiz;

	Scale escala;

	public PantallaFX(Group root, double resolucionW, double resolucionH) {
		super(resolucionW, resolucionH);
		setRoot(root);
//		canvas = new HashMap<Suelo, Canvas>();
		hoja = new Canvas(1500, 1000);
		lapiz = hoja.getGraphicsContext2D();
		root.getChildren().add(hoja);

		escala = new Scale(1, 1);
		root.getTransforms().setAll(escala);
	}

	public void agregar(Suelo suelo) {
//		Canvas c = new Canvas(1700, 1200);
//		c.setLayoutX(suelo.getX());
//		c.setLayoutY(suelo.getY());
//		root.getChildren().add(c);
//		canvas.put(suelo, c);

		suelo.colocar(this);
	}

	public Group getRoot() {
		return root;
	}

	public void setRoot(Group root) {
		this.root = root;
	}

	@Override
	public void dibujar(Suelo suelo) {

//		GraphicsContext lapiz = getLapis(suelo);
//		lapiz.clearRect(0, 0, lapiz.getCanvas().getWidth(), lapiz.getCanvas().getHeight());
		if (lapiz != null)
			lapiz.drawImage(suelo.getFondo().getFX(), suelo.getX() - getCamX(), suelo.getY() - getCamY());
		getSize().setX(root.getScene().getWidth());
		getSize().setY(root.getScene().getHeight());
		escala = new Scale(getTransformResolucionW(), getTransformResolucionH());
		root.getTransforms().setAll(escala);
	}

	@Override
	public void dibujarCuadricula(Suelo suelo) {

		Paint saveFill = lapiz.getFill();
		lapiz.setFill(new Color(1, 0, 0, 0.2));
		for (double x = -200; x < hoja.getWidth() + 200; x += Suelo.Xcuadrilla) {
			for (double y = -200; y < hoja.getHeight() + 200; y += Suelo.Ycuadrilla) {
				lapiz.strokeRect(x - getCamX() + suelo.getX(), y - getCamY() + suelo.getY(), 50, 50);
//				if (suelo.getOcupado(x, y))
//					lapiz.fillRect(x - getCamX() + suelo.getX(), y - getCamY() + suelo.getY(), 50, 50);
			}
		}
		suelo.getElementos().stream().filter(e -> e.getClass() == Obstaculo.class).forEach(e -> {
			if (((Obstaculo) e).isTangible())
				lapiz.fillRect(e.getIzq() - getCamX() + suelo.getX(), e.getSup() - getCamY() + suelo.getY(),
						e.getSize().getX(), e.getSize().getY());
		});
		lapiz.setFill(saveFill);

	}

//	@Override
//	public GraphicsContext getLapis(Suelo suelo) {
//		return canvas.get(suelo).getGraphicsContext2D();
//	}

	public void dibujar(Personaje personaje)

	{
		if (personaje.getMovimiento() == null) {
			Paint saveFill = lapiz.getFill();
			Paint saveStroke = lapiz.getStroke();

			lapiz.setFill(personaje.getDefaultColor());
			lapiz.fillOval(personaje.getGraficX(), personaje.getGraficY(), personaje.getSize().getX(),
					personaje.getSize().getY());

			lapiz.setFill(saveFill);
			lapiz.setStroke(saveStroke);
		} else
			try {
				Image imagen;
				Double angulo = null;
				if (personaje.getDestino() != null)
					angulo = personaje.calcularAngulo(personaje.getDestino());

				imagen = personaje.getMovimiento().getImage(angulo);
				lapiz.drawImage(imagen.getFX(), personaje.getGraficX(), personaje.getGraficY());
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	public void dibujar(Elemento elemento) {
		if (elemento.getImagen() != null)
			lapiz.drawImage(elemento.getImagen().getFX(), elemento.getGraficX(), elemento.getGraficY());

//		getLapis(elemento.getSuelo()).drawImage(elemento.getImagen().getFX(),
//				elemento.getX() - elemento.getSize().getX() / 2, elemento.getY() - elemento.getSize().getY() / 2);

	}

	public void dibujar(Mundo mundo) {

//		lapiz.drawImage(mundo.getSuelo().getFondo().getFX(), -getCamX(), -getCamY());
//		for (Elemento elemento : mundo.getSuelo().getElementos()) {
//			elemento.actualizar();
//		}

	}

	@Override
	public void limpiar() {
		lapiz.clearRect(0, 0, lapiz.getCanvas().getWidth(), lapiz.getCanvas().getHeight());

	}

	@Override
	public void dibujarRuta(Personaje personaje) {
		if (personaje.getDestino() != null) {
			Coords origen = new Coords(personaje.getDestino().getX() + personaje.getSuelo().getX(),
					personaje.getDestino().getY() + personaje.getSuelo().getY());

			dibujarLinea(personaje.getX() + personaje.getSuelo().getX(), personaje.getY() + personaje.getSuelo().getY(),
					origen.getX(), origen.getY());
			for (Coords destino : personaje.getRuta()) {
				dibujarLinea(origen.getX(), origen.getY(), destino.getX(), destino.getY());
				origen = destino;
			}
		}

	}

	@Override
	public void dibujarLinea(double a1, double a2, double b1, double b2) {
		lapiz.strokeLine(a1 - getCamX(), a2 - getCamY(), b1 - getCamX(), b2 - getCamY());

	}

	public void dibujar(Npc npc) {

		Paint saveFill = lapiz.getFill();
//		Paint saveStroke = lapiz.getStroke();
//
//		lapiz.setFill(npc.getDefaultColor());
//		lapiz.fillOval(npc.getGraficX(), npc.getGraficY(), npc.getSize().getX(), npc.getSize().getY());
		dibujar((Personaje) npc);
		if (((Personaje) npc).getClan().equals("Jugador"))
			lapiz.setFill(Color.GREEN);
		else
			lapiz.setFill(Color.RED);
		if (npc.getMaxHP() != npc.getHp() || ((Personaje) npc).getClan().equals("Jugador"))
			lapiz.fillRect(npc.getGraficX(), npc.getGraficY() - 15, npc.getSize().getX() / npc.getMaxHP() * npc.getHp(),
					4);

		lapiz.setFill(saveFill);
//		lapiz.setStroke(saveStroke);
	}

	public Canvas getHoja() {
		return hoja;
	}

//	public void dibujarHud ( int resistencia) {
//		int ancho = 100 * (resistencia / 300);
//		lapiz.strokeRect(19, 99, 102, 17);
//		lapiz.setStroke(Color.WHITE);
//		lapiz.setLineWidth(5);
//		lapiz.strokeLine(20, 100, ancho, 102);;
//		lapiz.setFill(Color.RED);
//		
//		
//	}

}
