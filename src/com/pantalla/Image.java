package com.pantalla;

public class Image {

	private String imagen;
	private double width;
	private double heigth;
	private javafx.scene.image.Image imagenFX;
	
	public Image(String imagen, double width, double heigth) {
		super();
		this.imagen = imagen;
		this.width = width;
		this.heigth = heigth;
		imagenFX = new javafx.scene.image.Image(imagen,width,heigth,false,false);
	}
	
	public javafx.scene.image.Image getFX() {
		return imagenFX;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeigth() {
		return heigth;
	}

	public void setHeigth(double heigth) {
		this.heigth = heigth;
	}
	
	
	
	
}
