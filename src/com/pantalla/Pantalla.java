package com.pantalla;

import com.elementos.Elemento;
import com.elementos.Npc;
import com.elementos.Obstaculo;
import com.elementos.Personaje;
import com.elementos.Suelo;
import com.tool.Coords;


public abstract class Pantalla {

	private Coords camera;

	private Coords resolucion;
	private Coords size;

	public Pantalla(double resolucionW, double resolucionH) {
		super();

		resolucion = new Coords(resolucionW, resolucionH);
		size = new Coords(resolucionW, resolucionH);

		camera = new Coords(0, 0);
	}

	public double getTransformResolucionW() {
		return size.getX() / resolucion.getX();
	}

	public double getTransformResolucionH() {

		return size.getY() / resolucion.getY();
	}

	public Coords getResolucion() {
		return resolucion;
	}

	public void setResolucion(Coords resolucion) {
		this.resolucion = resolucion;
	}

	public Coords getSize() {
		return size;
	}

	public void setSize(Coords size) {
		this.size = size;
	}

	public void moveCam(double x, double y) {
		camera.addX(x);
		camera.addY(y);
	}

	public double getCamX() {
		return camera.getX();

	}

	public double getCamY() {
		return camera.getY();
	}

//	public double getGX(Suelo suelo) {
//		return camera.getX() + suelo.getX();
//
//	}
//
//	public double getGY(Suelo suelo) {
//		return camera.getY() + suelo.getY();
//	}

	public abstract void agregar(Suelo suelo);

	public abstract void dibujar(Suelo suelo);

	public abstract void dibujarLinea(double a1, double a2, double b1, double b2);

	public abstract void dibujarCuadricula(Suelo suelo);

	public abstract void dibujarRuta(Personaje personaje);

	public abstract void dibujar(Personaje personaje);

	public abstract void dibujar(Elemento elemento);
	
	public abstract void dibujar(Npc npc);

	public abstract void limpiar();
}
