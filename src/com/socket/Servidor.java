package com.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Servidor extends ServerSocket {

	List<Socket> clientes;

	BufferedReader entradaBuff;
	BufferedWriter salidaBuff;

	public Servidor(int puerto) throws IOException {
		super(puerto);
		clientes = new ArrayList<Socket>();

	}

	public void waitConection(EventoSocket evento) {
		Thread recibirConexiones = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Socket cliente = receiveConection();
						Thread procesarCliente = new Thread(new Runnable() {

							@Override
							public void run() {
								boolean salir = false;
								while (!salir) {
									Object objetoRecibido;
									try {
										objetoRecibido = receiveObject(cliente);
										evento.run(objetoRecibido,cliente);
									} catch (IOException e) {
										System.err.println("Se perdio la conexion");
										StackTraceElement[] stack = e.getStackTrace();
										System.err.println(stack[stack.length-2]);
										clientes.remove(cliente);
										salir = true;
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

							}
						});

						procesarCliente.setDaemon(true);
						procesarCliente.start();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		});
		recibirConexiones.setDaemon(true);
		recibirConexiones.start();
	}

	public Socket receiveConection() throws IOException {
		Socket cliente = accept();
		clientes.add(cliente);
		return cliente;
	}

	public Object receiveObject(Socket cliente) throws ClassNotFoundException, IOException {
			ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
			return entrada.readObject();
	}


	public void sendObject(Socket cliente, Object objeto) throws IOException {
		ObjectOutputStream salida;
		salida = new ObjectOutputStream(cliente.getOutputStream());
		salida.writeObject(objeto);
	}

	public void sendObject(Object objeto) throws IOException {
		for (Socket cliente:clientes) {
			sendObject(cliente,objeto);
		}
	}
}
