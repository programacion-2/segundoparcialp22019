package com.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente extends Socket {
	public Cliente(String host, int port) throws UnknownHostException, IOException {
		super(host, port);
	}
	public void sendObject(Object o) {
		ObjectOutputStream salida;
		try {
			salida = new ObjectOutputStream(getOutputStream());
			salida.writeObject(o);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Object receiveObject() {
		try {
			ObjectInputStream entrada = new ObjectInputStream(getInputStream());
			return entrada.readObject();
			
			
		} catch (IOException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
			return new Error("Algo salio to mal");
		}
}
	public void waitObject(EventoSocket evento) {
		Thread recibirObjetos = new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(!Cliente.this.isClosed()) {
					Object recibido = receiveObject();
					evento.run(recibido,Cliente.this);
				}
				
			}
		});
		recibirObjetos.setDaemon(true);
		recibirObjetos.start();
	}
	
	
	
}
