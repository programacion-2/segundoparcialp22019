package com.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.login.bean.Usuario;

public class TablaUsuarios extends DBTabla<Usuario> {

	public TablaUsuarios() {
		super("usuarios");

		this.addColumnas(new Columna("ID", TipoDatoEnum.INT, 11, "NOT NULL AUTO_INCREMENT"));
		this.addColumnas(new Columna("name", TipoDatoEnum.VARCHAR, 20, "NOT NULL"));
		this.addColumnas(new Columna("pass", TipoDatoEnum.VARCHAR, 200, "NOT NULL"));
		this.addColumnas(new Columna("email",TipoDatoEnum.VARCHAR,20,"NOT NULL"));

		this.setPrimaryKey("ID");
		try {
			this.crearDB();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Usuario> leer() {

		try {
			return super.leer();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Usuario interpretar(ResultSet rs) throws SQLException
	//,InvalidNameException, InvalidPassException 
	{
		Usuario usuario = new Usuario(rs.getString("name"), rs.getString("pass"),rs.getString("email"));
//		usuario.setPass("", rs.getString("pass"));
		return usuario;
	}

	@Override
	public String registrotoString(Usuario usuario) {
		return "NULL,'"+ usuario.getUsuario() + "', '" + usuario.getClave() +"', '" + usuario.getEmail() + "'";
	}

}




