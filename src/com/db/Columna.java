package com.db;

public class Columna {
	private String name;
	private TipoDatoEnum tipo;
	private int size;
	private String extra;

	public Columna(String name, TipoDatoEnum tipo, int size, String extra) {
		super();
		this.name = name;
		this.tipo = tipo;
		this.size = size;
		this.extra = extra;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TipoDatoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoDatoEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
//		`ID` INT(11) NOT NULL AUTO_INCREMENT
		if (size > 0)
			return "`" + name + "` " + tipo + "(" + size + ") " + extra + " ";
		return "`" + name + "` " + tipo +" "+ extra + " ";
	}

}
