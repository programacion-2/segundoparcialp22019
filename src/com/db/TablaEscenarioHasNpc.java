package com.db;

import java.sql.ResultSet;
import java.util.List;

import com.elementos.Npc;
import com.elementos.Suelo;
import com.rpg.constructor.ManejadorNPC;

public class TablaEscenarioHasNpc extends DBTabla<Npc> {
	final static String nombreTabla = "Escenario_has_npc";
	private ManejadorNPC generico;

	public TablaEscenarioHasNpc() {
		super(TablaEscenarioHasNpc.nombreTabla);

		generico = new ManejadorNPC();

		this.addColumnas(new Columna("ID", TipoDatoEnum.INT, 11, "NOT NULL AUTO_INCREMENT"));
		this.addColumnas(new Columna("Escenarios_idEscenario", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 40, "NOT NULL"));
		this.addColumnas(new Columna("PosX", TipoDatoEnum.DOUBLE, 0, "NULL"));
		this.addColumnas(new Columna("PosY", TipoDatoEnum.DOUBLE, 0, "NULL"));

		this.setPrimaryKey("ID");
		try {
			this.crearDB();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Npc interpretar(ResultSet rs) throws Exception {
		Npc npc = generico.nuevo(rs.getString(getColumna(2))).crear(rs.getDouble(getColumna(3)),
				rs.getDouble(getColumna(4)));

		return npc;
	}

	public void cargarSuelo(Suelo suelo) {
		try {
			List<Npc> lista = buscar(getColumna(1) + " = '" + suelo.getId() + "'");
			lista.forEach(npc -> npc.colocar(suelo));

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public String registrotoString(Npc registro) {

		return "NULL,'" + registro.getSuelo().getId() + "', '" + registro.getKey() + "', '" + registro.getX() + "', '"
				+ registro.getY() + "'";
	}

	public ManejadorNPC getGenerico() {
		return generico;
	}

}