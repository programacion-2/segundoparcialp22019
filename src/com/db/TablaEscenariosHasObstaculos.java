package com.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.elementos.Obstaculo;

public class TablaEscenariosHasObstaculos extends DBTabla<Obstaculo> {
	final static String nombreTabla = "Escenarios_has_Obstaculos";
	private TablaObstaculos generico;

	public TablaEscenariosHasObstaculos() {
		super(TablaEscenariosHasObstaculos.nombreTabla);

		generico = new TablaObstaculos();

		this.addColumnas(new Columna("ID", TipoDatoEnum.INT, 11, "NOT NULL AUTO_INCREMENT"));
		this.addColumnas(new Columna("Escenarios_idEscenario", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Obstaculos_idObstaculo", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("PosX", TipoDatoEnum.DOUBLE, 11, "NULL"));
		this.addColumnas(new Columna("PosY", TipoDatoEnum.DOUBLE, 11, "NULL"));
		this.addColumnas(new Columna("SizeX", TipoDatoEnum.DOUBLE, 11, "NULL"));
		this.addColumnas(new Columna("SizeY", TipoDatoEnum.DOUBLE, 11, "NULL"));

//			// TODO para crear la tabla aca falta las claves foraneas
		this.setPrimaryKey("ID");
//		try {
//			this.crearDB();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

	}

	@Override
	public Obstaculo interpretar(ResultSet rs) throws Exception {
		Obstaculo l = generico.buscarId(rs.getInt(getColumna(2)));
		
		Obstaculo registro =new Obstaculo(l.getUrl(),
				rs.getDouble(getColumna(3)), rs.getDouble(getColumna(4)), rs.getDouble(getColumna(5)), rs.getDouble(getColumna(6)));
		registro.setID(rs.getInt(getColumna(0)));
		registro.setNombre(l.getNombre());
		return registro;
	}


	@Override
	public String registrotoString(Obstaculo registro) {
		
		return "NULL,'"+registro.getSuelo().getId()+"', '"+registro.getID()+"', '"+registro.getX()+"', '"+registro.getY()+"', '"+registro.getSize().getX()+"', '"+registro.getSize().getY()+"'";
	}
	
	

}
