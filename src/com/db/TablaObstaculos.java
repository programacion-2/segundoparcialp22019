package com.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elementos.Obstaculo;

public class TablaObstaculos extends DBTabla<Obstaculo> {

	final static String nombreTabla = "Obstaculos";

	Map<Integer, Obstaculo> cargados;

	public TablaObstaculos() {
		super(TablaObstaculos.nombreTabla);

		this.addColumnas(new Columna("idObstaculo", TipoDatoEnum.INT, 11, "NOT NULL AUTO_INCREMENT"));
		this.addColumnas(new Columna("Imagen", TipoDatoEnum.VARCHAR, 45, "NOT NULL"));
		this.addColumnas(new Columna("SizeX", TipoDatoEnum.DOUBLE, 11, "NULL"));
		this.addColumnas(new Columna("SizeY", TipoDatoEnum.DOUBLE, 11, "NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 45, "NULL"));

		this.setPrimaryKey("idObstaculo");
		try {
			this.crearDB();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cargados = new HashMap<Integer, Obstaculo>();

	}

	@Override
	public Obstaculo interpretar(ResultSet rs) throws Exception {

		Obstaculo registro = new Obstaculo(rs.getString(getColumna(1)), rs.getDouble(getColumna(2)),
				rs.getDouble(getColumna(3)));
		registro.setNombre(rs.getString(getColumna(4)));
		registro.setID(rs.getInt(getColumna(0)));
		return registro;
	}

	@Override
	public String registrotoString(Obstaculo registro) {

		return "NULL, '" + registro.getUrl() + "', '" + registro.getSize().getX() + "', '" + registro.getSize().getY()
				+ "', '" + registro.getNombre()+ "'";
	}

	public Obstaculo buscarId(int id) throws Exception {

		return cargados.getOrDefault(id, buscar("`idObstaculo` = '" + id + "'").get(0));
	}

	@Override
	public List<Obstaculo> buscar(String filtro) throws Exception {

		List<Obstaculo> lista = super.buscar(filtro);
		lista.forEach(o -> cargados.put(o.getID(), o));
		return lista;
	}

}
