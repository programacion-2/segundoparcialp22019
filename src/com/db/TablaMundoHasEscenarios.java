package com.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.elementos.Mundo;
import com.elementos.Suelo;
import com.pantalla.Image;
import com.rpg.ManejadorDB;

import javafx.util.Pair;

public class TablaMundoHasEscenarios extends DBTabla<Suelo> {
	final static String nombreTabla = "Mundo_has_Escenarios";

	Map<Integer, Mundo> mundos;

	public TablaMundoHasEscenarios() {
		super(TablaMundoHasEscenarios.nombreTabla);

		mundos = new HashMap<Integer, Mundo>();

		this.addColumnas(new Columna("ID", TipoDatoEnum.INT, 11, "NOT NULL AUTO_INCREMENT"));
		this.addColumnas(new Columna("Escenarios_idEscenario", TipoDatoEnum.INT, 11, "NULL"));
		this.addColumnas(new Columna("PosX", TipoDatoEnum.INT, 11, "NULL"));
		this.addColumnas(new Columna("PosY", TipoDatoEnum.INT, 11, "NULL"));
		this.addColumnas(new Columna("Imagen", TipoDatoEnum.VARCHAR, 100, "NOT NULL DEFAULT 'file:recursos/imagenes/grass_large_hipass_small.jpg'"));

//		// TODO para crear la tabla aca falta las claves foraneas
		this.setPrimaryKey("ID");
	
	}

	public void addMundo(Mundo mundo) {
		mundos.put(mundo.getId(), mundo);
	}

	@Override
	public Suelo interpretar(ResultSet rs) throws Exception {
		try {
			Suelo nuevo = mundos.get(rs.getInt(getColumna(0))).nuevoSuelo(rs.getInt(getColumna(1)),
					new Pair<Integer, Integer>(rs.getInt(getColumna(2)), rs.getInt(getColumna(3))));
			nuevo.setFondo(new Image(rs.getString(getColumna(4)), nuevo.width, nuevo.heigth));
			return nuevo;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String registrotoString(Suelo registro) {
		return "'" + registro.getMundo().getId() + "', NULL, '" + registro.getPos().getKey() + "', '"
				+ registro.getPos().getValue() +"', '" +registro.getFondo().getImagen()+ "'";
	}
}
