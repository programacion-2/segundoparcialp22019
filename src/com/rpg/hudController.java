package com.rpg;

import com.elementos.Personaje;

import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class hudController {

	@FXML
	private ImageView GeneralView;

	@FXML
	private Circle hp;
	@FXML
	private Circle mana;
	@FXML
	private Rectangle stamina;
	@FXML
	private Rectangle poow;

	@FXML
	private Group habilidadSelect;
	@FXML
	private Group habilidadView;

	@FXML
	private Circle personajestat;
	@FXML
	private Circle inventario;

	private double anchoPredStamina;

	private int habilidadSelecionada;

	public void initialize() {
		anchoPredStamina = stamina.getWidth() / 100;
		setHabilidadSelecionada(0);
	}

	public void setHp(double hp) {
		if (hp <= 0)
			hp = 0.001;
		RadialGradient gradient1 = new RadialGradient(0, .1, this.hp.getCenterX(), this.hp.getCenterY(), hp, false,
				CycleMethod.NO_CYCLE, new Stop(0, Color.RED), new Stop(1, Color.BLACK));
		this.hp.setFill(gradient1);
	}

	public void setMana(double mana) {
		if (mana <= 0)
			mana = 0.001;
		RadialGradient gradient2 = new RadialGradient(0, .1, this.mana.getCenterX(), this.mana.getCenterY(), mana,
				false, CycleMethod.NO_CYCLE, new Stop(0, Color.BLUE), new Stop(1, Color.BLACK));
		this.mana.setFill(gradient2);
	}

	public void setStamina(double stamina) {
		this.stamina.setWidth(stamina * anchoPredStamina);

	}

//	public void setPoow(double poow) {
//		if (poow <= 0)
//			poow = 0.001;
//	}

	public int getHabilidadSelect() {

		return habilidadSelecionada;
	}

	public void setHabilidadSelecionada(int habilidadSelecionada) {
		this.habilidadSelecionada = habilidadSelecionada;
		if (habilidadSelect != null)
			for (int i = 0; i < 4; i++) {
				if (i != habilidadSelecionada)
					habilidadSelect.getChildren().get(i).setOpacity(0);
				else
					habilidadSelect.getChildren().get(i).setOpacity(1);
			}

	}

	public void cargarHabilidades(Personaje personaje) {
		for (int i = 0; i < 4; i++) {
			ImageView view = (ImageView) habilidadView.getChildren().get(i);
			if (personaje.getHabilidad(i) != null)
				view.setImage(personaje.getHabilidad(i).getImagen().getFX());

		}

	}

	public ImageView getGeneralView() {

		return GeneralView;
	}

	public void presionado(EventTarget target) {
		for (int i = 0; i < 4; i++) {
			if (target.equals(habilidadSelect.getChildren().get(i)))
				setHabilidadSelecionada(i);
		}
		if (target.equals(personajestat))
			System.out.println("abriendo personaje");
		if (target.equals(inventario))
			System.out.println("abriendo inventario");

	}

}
