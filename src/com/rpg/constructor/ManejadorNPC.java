package com.rpg.constructor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import com.elementos.Elemento;
import com.elementos.Enemigo;
import com.elementos.Npc;
import com.elementos.Obstaculo;
import com.elementos.Personaje;
import com.elementos.Script;
import com.elementos.Suelo;
import com.escenas.Aplicacion;
import com.escenas.StageRPGConstructor;
import com.escenas.TextSelector;
import com.rpg.ManejadorDB;
import com.rpg.constructor.ManejadorHabilidades.LanzadorHabilidad;
import com.tool.Coords;

import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ManejadorNPC {
	private Map<String, CreadorNPC> creadores;

	private static ManejadorNPC _instance;

	public static ManejadorNPC instance() {
		if (_instance == null)
			_instance = new ManejadorNPC();
		return _instance;
	}

	public interface CreadorNPC {
		Npc crear(double x, double y);
	}

	public ManejadorNPC() {
		creadores = new HashMap<String, ManejadorNPC.CreadorNPC>();
		agregarVolador();
		agregarDeambulante();
		agregarDeambulanteS();
		agregarKamikaze();
		agregarAbrirPuertasAlFinal();
		agregarRodeadoDeArboles();
		agregarHabilidadInvocacionDeAmbulante();
		agregarRodeadoDePiedras();
		agregarBuff();
		agregarCheckPoint();
		agregarVictoria();
	}

	private void agregarRodeadoDeArboles() {
		creadores.put("RodeadoDeArboles", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();

				npc.setPos(x, y);
				npc.setSize(new Coords(0, 0));
				npc.setNombre("RodeadoDeArboles");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						System.out.println("rodeando de arboles:" + npc.getSuelo().getId());
						ManejadorObstaculos.rodearDeArboles(npc.getSuelo());
						npc.desaparecer();
					}
				});
				return npc;
			}
		});

	}

	private void agregarRodeadoDePiedras() {
		creadores.put("RodeadoDePiedras", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();

				npc.setPos(x, y);
				npc.setSize(new Coords(0, 0));
				npc.setNombre("RodeadoDePiedras");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						System.out.println("rodeando de piedras:" + npc.getSuelo().getId());
						ManejadorObstaculos.rodearDePiedras(npc.getSuelo());
						npc.desaparecer();
					}
				});
				return npc;
			}
		});

	}

	private void agregarAbrirPuertasAlFinal() {
		creadores.put("AbrirPuertasAlMorirTodos", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();
				npc.setPos(x, y);
				npc.setSize(new Coords(0, 0));
				npc.setClan("Escenario");
				npc.setNombre("AbrirPuertasAlFinal");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						if (npc.getSuelo().getElementos().stream().filter(e -> Enemigo.class.isInstance(e))
								.filter(p -> !((Enemigo) p).getClan().equals("Jugador")).count() == 0
								|| npc.getSuelo().getElementos().stream().filter(e -> e.getClass() == Personaje.class)
										.count() == 0) {
							try {
								npc.getSuelo().getElementos().stream().filter(e -> {
									if (e.getNombre() == null)
										return false;
									return e.getNombre().equals("PuertaDeEnemigosMuertos");
								}).forEach(puerta -> ((Obstaculo) puerta).setIntangible(true));

							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							Personaje personaje = (Personaje) npc.getSuelo().getElementos().stream()
									.filter(e -> e.getClass() == Personaje.class).findFirst().get();
							try {
								npc.getSuelo().getElementos().stream().filter(e -> {
									if (e.getNombre() == null)
										return false;
									return e.getNombre().equals("PuertaDeEnemigosMuertos");
								}).forEach(puerta -> {

									((Obstaculo) puerta).setIntangible(false);
									npc.buscarPersonajes().forEach(
											p -> ((Obstaculo) puerta).setIntangible(!((Obstaculo) puerta).isTangible()
													|| puerta.getHitPoint(p.getX(), p.getY(), true)));
								});

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					}
				});
				return npc;
			}
		});

	}

	private void agregarVictoria() {
		creadores.put("Victoria", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();
				npc.setPos(x, y);
				npc.setSize(new Coords(0, 0));
				npc.setClan("Escenario");
				npc.setNombre("Victoria");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						if (npc.getSuelo().getElementos().stream().filter(e -> Enemigo.class.isInstance(e))
								.filter(p -> !((Enemigo) p).getClan().equals("Jugador")).count() == 0) {
							try {
								FXMLLoader loader = new FXMLLoader();
								loader.setLocation(Aplicacion.class.getResource("Victoria.fxml"));

								
									Scene victoria = new Scene(loader.load());
									Stage ventana = new Stage();
									ventana.setScene(victoria);
									ventana.show();
									npc.morir();
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						} 
					}
				});
				return npc;
			}
		});

	}

	private void agregarHabilidadInvocacionDeAmbulante() {
		creadores.put("HabilidadInvocacionDeAmbulante", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();
				npc.setPos(x, y);
				npc.setSize(new Coords(30, 30));
				npc.setMovimiento("file:recursos/imagenes/imagenMovimientoDeambulante", 4);
				npc.setHp(0d);
				npc.setClan("Escenario");
				npc.setNombre("HabilidadInvocacionDeAmbulante");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						Personaje jugador = StageRPGConstructor.instance().getPersonaje();
						if (jugador.getPos().distancia(npc.getPos()) < (jugador.getSize().getX() + npc.getSize().getX())
								/ 2)
							jugador.addHabilidad(ManejadorHabilidades.instance().get("InvocarDeambulante"));
						StageRPGConstructor.instance().getControladorHud().cargarHabilidades(jugador);
					}
				});
				return npc;
			}
		});

	}

	private void agregarBuff() {
		creadores.put("Buff", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();
				npc.setPos(x, y);
				npc.setSize(new Coords(300, 300));
				npc.setMovimiento("file:recursos/imagenes/imagenMovimientoKamikaze", 4);
				npc.setClan("Escenario");
				npc.setNombre("Buff");
				npc.setHp(0d);
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						Personaje jugador = StageRPGConstructor.instance().getPersonaje();
						if (jugador.getPos().distancia(npc.getPos()) < (jugador.getSize().getX() + npc.getSize().getX())
								/ 2) {
							jugador.buff();
							npc.morir();
						}
					}
				});
				return npc;
			}
		});

	}

	private void agregarCheckPoint() {
		creadores.put("CheckPoint", new CreadorNPC() {

			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Npc();
				npc.setPos(x, y);
				npc.setSize(new Coords(0, 0));
				npc.setClan("Escenario");
				npc.setNombre("CheckPoint");
				npc.setIa(new Script() {

					@Override
					public void run(double time) {
						try {
							Personaje personaje = (Personaje) npc.getSuelo().getElementos().stream()
									.filter(e -> e.getClass() == Personaje.class).findFirst().get();
							if (personaje != null)
								personaje.setCheckpoint(npc.getPos(), npc.getSuelo());
						} catch (Exception e) {
						}
					}
				});
				return npc;
			}
		});

	}

	private void agregarDeambulanteS() {
		creadores.put("DeambulanteSpawner", new CreadorNPC() {
			@Override
			public Npc crear(double x, double y) {
				Npc spawner = new Enemigo();
				spawner.setDefaultColor(Color.DARKRED);
				spawner.setSize(new Coords(75, 75));
				spawner.setAtaque(30);
				spawner.setNombre("SpawnerDeDeambulantes");
				spawner.setRecuperacionMana(10);
				spawner.setIa(new Script() {

					@Override
					public void run(double t) {
						if (spawner.getHp() < 0)
							spawner.morir();
						if (spawner.getMana() > 100) {
							Npc npc = ManejadorNPC.instance().nuevo("Deambulante").crear(spawner.getX(),
									spawner.getY());
							npc.setClan(spawner.getClan());
							npc.colocar(spawner.getSuelo());
							spawner.setMana(0);
						}

					}
				});
				spawner.setPos(x, y);
				return spawner;
			}
		});
	}

	private void agregarVolador() {
		creadores.put("Volador", new CreadorNPC() {
			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Enemigo();
				npc.addHabilidad(ManejadorHabilidades.instance().get("disparoBasico"));
				npc.setRecuperacionMana(10);// 10
				npc.setMana(0);
				npc.setNombre("Volador");
				npc.setSize(new Coords(70, 70));
				npc.setMovimiento("file:recursos/imagenes/imagenMovimientoVolador", 4);
				npc.setIa(new Script() {

					@Override
					public void run(double t) {

						if (npc.getDestino() == null)
							try {
								Elemento aSeguir = npc.buscarAliados().findFirst().get();
								npc.setDestino(new Coords(aSeguir.getAbsX(), aSeguir.getAbsY() + 10));

								Elemento objetivo = npc.buscarEnemigos().findFirst().get();
								LanzadorHabilidad habilidad = npc.getHabilidad(0);
								if (habilidad != null) {
									Coords disparar = new Coords(objetivo.getAbsX(), objetivo.getAbsY());
									habilidad.lanzar(npc, disparar);
								}

							} catch (NoSuchElementException ex) {
							}
						if (npc.getHp() < 0)
							npc.morir();
					}
				});
				npc.setPos(x, y);
				return npc;
			}
		});

	}

	private void agregarDeambulante() {
		creadores.put("Deambulante", new CreadorNPC() {
			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Enemigo();
				npc.setAtaque(20);
				npc.setRecuperacionMana(10);// 10
				npc.setMana(0);
				npc.setSize(new Coords(80, 80));
				npc.setMovimiento("file:recursos/imagenes/imagenMovimientoDeambulante", 3);
				npc.setNombre("Deambulante");
				npc.setIa(new Script() {

					@Override
					public void run(double t) {

						if (npc.getDestino() == null)
							npc.setDestino(new Coords(Math.random() * 200 - 100 + npc.getAbsX(),
									Math.random() * 200 - 100 + npc.getAbsY()));
						if (npc.getHp() < 0)
							npc.morir();
						if (npc.getMana() > 150) {
							Npc npc2 = ManejadorNPC.this.nuevo("Kamikaze").crear(npc.getX(), npc.getY());
							npc2.setClan(npc.getClan());
							npc2.colocar(npc.getSuelo());
							npc.setMana(0);

						}
					}
				});
				npc.setPos(x, y);
				return npc;
			}
		});

	}

	private void agregarKamikaze() {
		creadores.put("Kamikaze", new CreadorNPC() {
			@Override
			public Npc crear(double x, double y) {
				Npc npc = new Enemigo();
				npc.setNombre("Kamikaze");
				npc.setAtaque(5);
				npc.setSize(new Coords(40, 40));
				npc.setMovimiento("file:recursos/imagenes/imagenMovimientoKamikaze", 3);
				npc.setMaxHP(1);
				npc.setHp(1);
				npc.setDefaultColor(Color.BLUE);
				npc.setIa(new Script() {

					@Override
					public void run(double t) {

						if (npc.getDestino() == null)
							try {
								Elemento objetivo = npc.buscarEnemigos().findFirst().get();
//							
								npc.setDestino(new Coords(objetivo.getAbsX(), objetivo.getAbsY()));
							} catch (NoSuchElementException ex) {
							}
						if (npc.getHp() < 0)
							npc.morir();
					}
				});
				npc.setPos(x, y);
				return npc;
			}
		});

	}

	public CreadorNPC nuevo(String key) {
		return creadores.getOrDefault(key, creadores.get("Deambulante"));
	}

	public Map<String, CreadorNPC> getCreadores() {
		return creadores;
	}

	public void crearNPC(TextSelector<CreadorNPC> selector, double x, double y) {
		CreadorNPC nuevo = selector.getValue();
		if (nuevo != null) {
			Npc npc = nuevo.crear(x, y);
			npc.colocar(StageRPGConstructor.instance().getMundo().getSuelo());
			npc.getRuta().clear();
			npc.setKey(selector.getText());
			try {
				ManejadorDB.instance().getCargadorNPC().agregarRegistro(npc);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void cargarSuelo(Suelo suelo) {
		ManejadorDB.instance().getCargadorNPC().cargarSuelo(suelo);
	}

}
