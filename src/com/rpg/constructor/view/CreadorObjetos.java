package com.rpg.constructor.view;

import java.io.IOException;
import java.sql.SQLException;

import com.db.TablaMundoHasEscenarios;
import com.elementos.Npc;
import com.elementos.Obstaculo;
import com.elementos.Suelo;
import com.escenas.Aplicacion;
import com.escenas.StageRPGConstructor;
import com.escenas.TextSelector;
import com.rpg.constructor.ManejadorNPC;
import com.rpg.constructor.ManejadorNPC.CreadorNPC;
import com.tool.Coords;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

public class CreadorObjetos {

	@FXML
	private Stage subVentana;
	@FXML
	private TitledPane objetos;
	@FXML
	private TitledPane NPC;

	@FXML
	private TitledPane mapa;

	@FXML
	private TextField mPosX;
	@FXML
	private TextField mPosY;
	@FXML
	private TextField mImagen;

	private TextSelector<Obstaculo> selectObscaculo;
	private TextSelector<ManejadorNPC.CreadorNPC> selectNPC;

	@FXML
	private TextField sizeXObstaculo;

	@FXML
	private TextField sizeYObstaculo;

	@FXML
	private GridPane gridNPC;

	private Stage ventana;

	public static CreadorObjetos newInstance() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Aplicacion.class.getResource("../rpg/constructor/view/PanelObjetos.fxml"));
		loader.load();

		CreadorObjetos nuevo = loader.getController();

		return nuevo;
	}

	@FXML
	protected void initialize() {
		this.selectObscaculo = new TextSelector<Obstaculo>();
		GridPane.setConstraints(this.selectObscaculo, 0, 1);
		((GridPane) this.sizeXObstaculo.getParent()).getChildren().add(this.selectObscaculo);

		this.selectNPC = new TextSelector<ManejadorNPC.CreadorNPC>();
		GridPane.setConstraints(this.selectNPC, 0, 1);
		gridNPC.getChildren().add(this.selectNPC);

	}

	public TextSelector<ManejadorNPC.CreadorNPC> getSelectNPC() {
		return selectNPC;
	}

	public TextSelector<Obstaculo> getSelectObscaculo() {
		return selectObscaculo;
	}

	public TextField getSizeXObstaculo() {
		return sizeXObstaculo;
	}

	public Stage getVentana() {
		return ventana;
	}

	public void setVentana(StageRPGConstructor ventana) {
		this.ventana = ventana;
		subVentana.initOwner(ventana);
//		subVentana.initModality(Modality.WINDOW_MODAL);
		subVentana.setOnCloseRequest(e -> ventana.cerrarConstructor());
		subVentana.show();

	}

	public TextField getSizeYObstaculo() {
		return sizeYObstaculo;
	}

	public Stage getSubVentana() {
		return subVentana;
	}

	public Obstaculo getObstaculo() {
		Obstaculo obstaculo = selectObscaculo.getValue();
		if (obstaculo != null)
			obstaculo.setSize(new Coords(Double.parseDouble(sizeXObstaculo.getText()),
					Double.parseDouble(sizeYObstaculo.getText())));

		return obstaculo;
	}

	public TitledPane getPaneObjetos() {
		return objetos;
	}

	public TitledPane getPaneNPC() {
		return NPC;
	}

	public CreadorNPC getNPC() {
		return getSelectNPC().getValue();
	}

	@FXML
	public void botonAgregarMapa() {
		System.out.println("Agregando");
		TablaMundoHasEscenarios mundo = new TablaMundoHasEscenarios();
		try {
			Suelo sueloNuevo = new Suelo(null, StageRPGConstructor.instance().getMundo(),
					new Pair<Integer, Integer>(Integer.parseInt(mPosX.getText()), Integer.parseInt(mPosY.getText())));
			mundo.agregarRegistro(sueloNuevo);

		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
