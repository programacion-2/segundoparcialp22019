package com.rpg.constructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elementos.Elemento;
import com.elementos.Npc;
import com.elementos.Personaje;
import com.elementos.Script;
import com.elementos.Suelo;
import com.elementos.proyectiles.Colision;
import com.elementos.proyectiles.Proyectil;
import com.pantalla.Image;
import com.tool.Coords;

public class ManejadorHabilidades {
	private Map<String, LanzadorHabilidad> lanzadores;
	private static ManejadorHabilidades _instance = null;

	public abstract class LanzadorHabilidad {
		private Image imagen;

		public abstract Script lanzar(Personaje lanzador, Coords objetivo);

		public Image getImagen() {
			return imagen;
		}

		public LanzadorHabilidad(Image imagen) {
			this.imagen = imagen;
		}
	}

	private ManejadorHabilidades() {
		lanzadores = new HashMap<String, ManejadorHabilidades.LanzadorHabilidad>();
		agregarDisparoBasico();
		agregarInvocarDeambulante();
	}

	public static ManejadorHabilidades instance() {
		if (_instance == null)
			_instance = new ManejadorHabilidades();
		return _instance;
	}

	private void agregarDisparoBasico() {
		
		Image imagen= new Image("file:recursos/imagenes/habilidades/disparobasico.png", 55, 55);
		lanzadores.put("disparoBasico", new LanzadorHabilidad(imagen) {

			@Override
			public Script lanzar(Personaje lanzador,Coords objetivo) {
				if (lanzador.getMana() >= 20) {
					Proyectil p = new Proyectil(lanzador);
					p.setClan(lanzador.getClan());
					p.setColision(new Colision() {
						@Override
						public void hit(Elemento victima) {
							if (Personaje.class.isInstance(victima))
								if  (!((Personaje) victima).getClan().equals(p.getClan())) {
								((Personaje) victima).addHp(-10);
								p.desaparecer();}
						}
					});
					p.setDireccion(lanzador.calcularAngulo(objetivo), 5000);
					p.setDesaceleracion(50);
					p.colocar(lanzador.getSuelo());
					lanzador.addMana(-20);
							
				}
				return null;
			};

		});

	}
	
private void agregarInvocarDeambulante() {
		
		Image imagen= new Image("file:recursos/imagenes/imagenMovimientoDeambulante/Abajo1.png", 55, 55);
		lanzadores.put("InvocarDeambulante", new LanzadorHabilidad(imagen) {

			@Override
			public Script lanzar(Personaje lanzador, Coords objetivo) {
				if (lanzador.getMana()>=100) {
					Npc npc = ManejadorNPC.instance().nuevo("Deambulante").crear(lanzador.getX(), lanzador.getY());
				npc.setClan(lanzador.getClan());
				npc.colocar(lanzador.getSuelo());
				lanzador.addMana(-100);}
				return null;
			}
			
		});
		
		}

	public LanzadorHabilidad get(String name) {
		return lanzadores.get(name);
	}
}
