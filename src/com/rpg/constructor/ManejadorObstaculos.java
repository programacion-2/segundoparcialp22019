package com.rpg.constructor;

import com.db.TablaObstaculos;
import com.elementos.Obstaculo;
import com.elementos.Suelo;
import com.escenas.StageRPGConstructor;
import com.rpg.ManejadorDB;
import com.tool.Coords;

public class ManejadorObstaculos {
	private static ManejadorObstaculos _instance;

	public static ManejadorObstaculos instance() {
		if (_instance == null)
			_instance = new ManejadorObstaculos();
		return _instance;
	}

	private ManejadorObstaculos() {

	}

	public Obstaculo crearObstaculoRandom(double x, double y) {
		Obstaculo obstaculo = ManejadorDB.instance().obtenerObstaculoRandom();
		return crearObstaculo(obstaculo, x, y);

	}

	// crea y siempre los carga en la DB
	public Obstaculo crearObstaculo(Obstaculo obstaculo, double x, double y) {
		return crearObstaculo(obstaculo, x, y, true,true);
	}

	// crea onstaculos y los guarda si el parametro guardar=true
	public Obstaculo crearObstaculo(Obstaculo obstaculo, double x, double y, boolean guardar, boolean colocar) {
		Obstaculo nuevo = new Obstaculo(obstaculo.getUrl().toString(), x, y, obstaculo.getSize().getX(),
				obstaculo.getSize().getY());
		nuevo.setID(obstaculo.getID());
		nuevo.setNombre(obstaculo.getNombre());
		System.out.println("agregando " + nuevo + " ," + nuevo.getX() + ":" + nuevo.getY());
		if (colocar)
			nuevo.colocar(StageRPGConstructor.instance().getMundo().getSuelo());
		if (guardar)
			ManejadorDB.instance().guardarObstaculo(nuevo);
		return nuevo;
	}

	public static void rodearDeArboles(Suelo suelo) {
		rodearDeObstaculos(suelo, 1);
	}
	
	public static void rodearDePiedras(Suelo suelo) {
		rodearDeObstaculos(suelo, 4);
	}

	private static void rodearDeObstaculos(Suelo suelo, int i) {

		try {
			Obstaculo obstaculo = (new TablaObstaculos()).buscarId(i);
			lineaDeObstaculos(suelo, new Coords(50, 0), new Coords(50, Suelo.heigth), obstaculo);
			lineaDeObstaculos(suelo, new Coords(0, Suelo.heigth - 50), new Coords(Suelo.width, Suelo.heigth - 50),
					obstaculo);
			lineaDeObstaculos(suelo, new Coords(0, 50), new Coords(Suelo.width, 50), obstaculo);
			lineaDeObstaculos(suelo, new Coords(Suelo.width - 50, 0), new Coords(Suelo.width - 50, Suelo.heigth),
					obstaculo);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void lineaDeObstaculos(Suelo suelo, Coords inicio, Coords fin, Obstaculo obstaculo) {
		double separacion = 100;
		System.out.println((int) (inicio.distancia(fin) / separacion));
		System.out.println(inicio.calcularAngulo(fin));
		Coords pos = inicio.calcularDireccion(inicio.calcularAngulo(fin), separacion / 2);
		System.out.println(pos);
		for (int i = 0; i < (int) (inicio.distancia(fin) / separacion); i++) {
			suelo.soutPosString();
			if (!suelo.getHitPoint(pos.getX(), pos.getY(), true))
				ManejadorObstaculos.instance().crearObstaculo(obstaculo, pos.getX(),
						pos.getY(), false,false).colocar(suelo);
			pos = pos.calcularDireccion(inicio.calcularAngulo(fin), separacion);
		}
	}

}
