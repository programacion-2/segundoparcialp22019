package com.rpg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.TablaObstaculos;
import com.db.DataBase;
import com.db.TablaEscenarioHasNpc;
import com.db.TablaEscenariosHasObstaculos;
import com.elementos.Obstaculo;

public class ManejadorDB {

	List<Obstaculo> obstaculosGenericos;
	private TablaObstaculos dbObstaculos;
	private TablaEscenariosHasObstaculos dbEhO;
	private TablaEscenarioHasNpc cargadorNPC;
	private static ManejadorDB _instance;

	public static ManejadorDB instance() {
		if (_instance == null)
			_instance = new ManejadorDB();
		return _instance;
	}

	private ManejadorDB() {
		super();

		dbObstaculos = new TablaObstaculos();
		dbEhO = new TablaEscenariosHasObstaculos();
		cargadorNPC = new TablaEscenarioHasNpc();

		cargarObstaculosGenericos();
	}

	public List<Obstaculo> cargarObstaculosGenericos() {

		try {
			obstaculosGenericos = dbObstaculos.leer();
		} catch (Exception e) {
			obstaculosGenericos = new ArrayList<Obstaculo>();
			e.printStackTrace();
		}
		return obstaculosGenericos;

	}

	public List<Obstaculo> leerObstaculos(int suelo) {
		try {
			return dbEhO.buscar("`" + dbEhO.getColumna(1) + "`='" + suelo + "'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
		return new ArrayList<Obstaculo>();
	}

	public Obstaculo obtenerObstaculoRandom() {
		double ran = Math.random();
		int index = (int) (ran * obstaculosGenericos.size());

		return obtenerObstaculo(index);
	}

	public Obstaculo obtenerObstaculo(int index) {
		return obstaculosGenericos.get(index);
	}

	public void guardarObstaculo(Obstaculo nuevo) {
		try {
			dbEhO.agregarRegistro(nuevo);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public TablaEscenarioHasNpc getCargadorNPC() {
		return cargadorNPC;
	}

}
