package com.game1;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;


public class Pelota {
	private double x;
	private double y;
	private int dx = 1, dy = 1;
	private static final int TAMX = 15;
	private static final int TAMY = 15;
	private Integer score1 = 0, score2 = 0;
	public boolean finJuego = false;

	Audio golpe = new Audio("./recursos/audios/golpe.wav");
	Audio anotacion = new Audio("./recursos/audios/anotacion.wav");

	public Pelota(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public Rectangle2D getPelota() {
		return new Rectangle2D.Double(x, y, TAMX, TAMY);

	}

	public void mover(Rectangle limites, boolean colisionR1, boolean colisionR2) {
		x += dx;
		y += dy;
		if (colisionR1) {
			dx = -dx;
			x = 25;
			golpe.start();
		}
		if (colisionR2) {
			dx = -dx;
			x = 755;
			golpe.start();
		}

		if (x < limites.getMinX()) {
			score2++;

			x = limites.getCenterX();
			y = limites.getCenterY();
			dx = -dx;
			anotacion.start();

		}

		if (x + TAMX >= limites.getMaxX()) {
			score1++;

			x = limites.getCenterX();
			y = limites.getCenterY();
			dx = -dx;
			anotacion.start();

		}

		if (y < limites.getMinY()) {

			y = limites.getMinY();

			dy = -dy;
			golpe.start();

		}

		if (y + TAMY >= limites.getMaxY()) {

			y = limites.getMaxY() - TAMY;

			dy = -dy;
			golpe.start();

		}
	}

	public int getScore1() {
		return score1;
	}

	public int getScore2() {
		return score2;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setScore1(Integer score1) {
		this.score1 = score1;
	}

	public void setScore2(Integer score2) {
		this.score2 = score2;
	}
}
