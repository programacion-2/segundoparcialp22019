package com.game1;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.socket.Cliente;
import com.socket.EventoSocket;

public class TableroJuegoCliente extends TableroJuego {

	public TableroJuegoCliente() {
		super();
	}
	
	public void  conectar(String ipServer) {
		try {
			Cliente cliente = new Cliente(ipServer, 9595);
			cliente.sendObject("hola mundo");
			cliente.waitObject(new EventoSocket() {

				@Override
				public void run(Object objeto, Socket origen) {
					if (objeto.getClass() == EstadoGame.class) {
						EstadoGame estado = (EstadoGame) objeto;
						raqueta2.setY(estado.getRaqueta1());
						pelota.setX(estado.getPelotaX());
						pelota.setY(estado.getPelotaY());
						pausa=estado.isPausa();
						pelota.setScore2(estado.getScore1());
						pelota.setScore1(estado.getScore2());
					}

					
					
					
					if (pelota.getScore1() != 5 && pelota.getScore2() != 5)
					cliente.sendObject(raqueta1.getY());

				}
			});

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void actualizar() {

		raqueta1.moverR2(getBounds());
	}

}
