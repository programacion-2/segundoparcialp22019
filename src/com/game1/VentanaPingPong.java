package com.game1;

import javax.swing.JFrame;

public class VentanaPingPong extends JFrame {

	private TableroJuego canvas;

	public VentanaPingPong() {
		setTitle("Ping Pong Game");
		setSize(800, 500);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas = createGame();
		add(canvas);
		addKeyListener(new EventoTeclado());
		new Hilo(canvas).start();
	}

	protected TableroJuego createGame() {
		return new TableroJuego();
	}
	
	

}
