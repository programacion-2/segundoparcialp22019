package com.game1;

import java.awt.Graphics2D;
import java.io.IOException;
import java.net.Socket;

import com.socket.EventoSocket;
import com.socket.Servidor;

public class TableroJuegoServer extends TableroJuego {

	Socket cliente;
	Servidor server;

	public TableroJuegoServer() {
		super();
		pausa = true;
		try {
			server = new Servidor(9595);
			server.waitConection(new EventoSocket() {

				@Override
				public void run(Object objeto, Socket origen) {
					cliente = origen;
					if (objeto.getClass() == Integer.class)
						raqueta2.setY((Integer) objeto);
					else
						System.out.println(objeto);
				}
			});

			Thread transmitir = new Thread(new Runnable() {

				@Override
				public void run() {
					while (true) {
						System.out.println(".");
						if (cliente != null) {
							try {
								Thread.sleep(30);
								server.sendObject(cliente,
										new EstadoGame(786 - pelota.getX(), pelota.getY(), raqueta1.getY(),
												raqueta2.getY(), pausa, pelota.getScore1(), pelota.getScore2()));

							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
			 				} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}

				}
			});

			transmitir.start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void actualizar() {
		pelota.mover(getBounds(), colision(raqueta1.getRaqueta()), colision(raqueta2.getRaqueta()));
		raqueta1.moverR1(getBounds());
	}
}
