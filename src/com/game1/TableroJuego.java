package com.game1;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class TableroJuego extends JPanel {
	protected Pelota pelota = new Pelota(0, 0);
	protected Raqueta raqueta1 = new Raqueta(10, 200);
	protected Raqueta raqueta2 = new Raqueta(794 - 10 - Raqueta.ANCHO, 200);
	protected boolean pausa = false;


	public TableroJuego() {
		setBackground(Color.BLACK);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		dibujarPuntaje(g2);
		dibujar(g2);
		actualizar();
	}

	public void dibujar(Graphics2D g) {
		g.fill(pelota.getPelota());
		g.fill(raqueta1.getRaqueta());
		g.fill(raqueta2.getRaqueta());
	}

	public void actualizar() {
		pelota.mover(getBounds(), colision(raqueta1.getRaqueta()), colision(raqueta2.getRaqueta()));
		raqueta1.moverR1(getBounds());
		raqueta2.moverR2(getBounds());

	}

	protected boolean colision(Rectangle2D r) {
		return pelota.getPelota().intersects(r);

	}

	protected void dibujarPuntaje(Graphics2D g) {
		Graphics2D g1 = g, g2 = g;
		Font score = new Font("Arial", Font.BOLD, 30);
		g.setFont(score);

		g1.drawString(Integer.toString(pelota.getScore1()), (float) getBounds().getCenterX() - 50, 30);
		g2.drawString(Integer.toString(pelota.getScore2()), (float) getBounds().getCenterX() + 25, 30);
		if (pelota.getScore1() == 5) {
			g.drawString("GANA El JUGADOR 1", (float) getBounds().getCenterX() - 180,
					(float) getBounds().getCenterY() - 100);
			pelota.finJuego = true;
		}
		if (pelota.getScore2() == 5) {
			g.drawString("GANA EL JUGADOR 2", (float) getBounds().getCenterX() - 180,
					(float) getBounds().getCenterY() - 100);
			pelota.finJuego = true;
		}
	}
	
	public boolean isPausa() {
		return pausa;
	}

	public void setPausa(boolean pausa) {
		this.pausa = pausa;
	}

	public Pelota getPelota() {
		return pelota;
	}

}
