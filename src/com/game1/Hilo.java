package com.game1;


public class Hilo extends Thread {

	private final TableroJuego canvas;

	public Hilo(TableroJuego canvas) {
		this.canvas = canvas;	
		
	}

	@Override
	
	
	public void run() {
		while (!canvas.getPelota().finJuego) {
			while (EventoTeclado.p == true) {
				System.out.println("estas entrando en pausa");
				canvas.setPausa(true);
			}
			while (canvas.isPausa()) {

				System.out.println("estas en pausa");
				while (EventoTeclado.p == true) {

					System.out.println("estas saliendo en pausa");
					canvas.setPausa(false);
				}
			}
			
			canvas.repaint();
			try {
				// Paint Velocity
				Thread.sleep(4);
			} catch (Exception ex) {
				System.out.println("error in graphics engine: " + ex.getMessage());
			}
		}
	}
}
