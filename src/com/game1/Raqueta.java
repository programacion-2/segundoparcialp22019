package com.game1;

import java.awt.geom.Rectangle2D;
import java.io.Serializable;

public class Raqueta{

	private int x, y;
	static final int ANCHO = 10, ALTO = 40;

	public Raqueta(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Rectangle2D getRaqueta() {
		return new Rectangle2D.Double(x, y, ANCHO, ALTO);
	}

	public void moverR1(Rectangle2D limites) {
		if (EventoTeclado.w && y > limites.getMinY()) {
			y--;
		}
		if (EventoTeclado.s && y < limites.getMaxY() - ALTO) {
			y++;
		}

	}

	public void moverR2(Rectangle2D limites) {
		if (EventoTeclado.up && y > limites.getMinY()) {
			y--;
		}
		if (EventoTeclado.down && y < limites.getMaxY() - ALTO) {
			y++;
		}
	}
}
