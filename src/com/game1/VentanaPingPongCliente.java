package com.game1;

import javax.swing.JFrame;

public class VentanaPingPongCliente extends VentanaPingPong{
	String ip;
	TableroJuegoCliente tablero;

	public VentanaPingPongCliente(String ip) {
		super();
		tablero.conectar(ip);
	}
	

	protected TableroJuego createGame() {
		tablero =new TableroJuegoCliente();
		return tablero;
	}
}
