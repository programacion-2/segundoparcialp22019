package com.game1;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Audio {

	String ruta;
	Clip clip = null;
	File archivo;
	private AudioInputStream ais;
	private DataLine.Info info;

	public Audio(String ruta) {
		this.ruta = ruta;
		load();

	}

	public void start() {
		clip.start();
		load();
	}

	private void load() {
		try {

//    		InputStream is= ClassLoader.class.getResourceAsStream(ruta);
			archivo = new File(ruta);
//    		AudioInputStream ais= AudioSystem.getAudioInputStream( new BufferedInputStream(is));
			ais = AudioSystem.getAudioInputStream(archivo);
			info = new DataLine.Info(Clip.class, ais.getFormat());
			clip = (Clip) AudioSystem.getLine(info);

			clip.open(ais);

		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
