package com.game1;

import java.io.Serializable;

public class EstadoGame implements Serializable {

	private double pelotaX;
	private double pelotaY;
	private int raqueta1;
	private int raqueta2;
	private boolean pausa;
	private int score1;
	private int score2;
	
	

	
	
	public EstadoGame(double pelotaX, double pelotaY, int raqueta1, int raqueta2, boolean pausa, int score1,
			int score2) {
		super();
		this.pelotaX = pelotaX;
		this.pelotaY = pelotaY;
		this.raqueta1 = raqueta1;
		this.raqueta2 = raqueta2;
		this.pausa = pausa;
		this.score1 = score1;
		this.score2 = score2;
	}
	public double getPelotaX() {
		return pelotaX;
	}
	public double getPelotaY() {
		return pelotaY;
	}
	public int getRaqueta1() {
		return raqueta1;
	}
	public int getRaqueta2() {
		return raqueta2;
	}
	public boolean isPausa() {
		return pausa;
	}
	public int getScore1() {
		return score1;
	}
	public int getScore2() {
		return score2;
	}
	
	

}
