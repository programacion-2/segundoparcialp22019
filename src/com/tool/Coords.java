package com.tool;

public class Coords {
	private double x;
	private double y;

	public Coords(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void addX(double x) {
		this.x += x;
	}

	public void addY(double y) {
		this.y += y;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "(" + getX() + "," + getY() + ")";
	}

	public Coords calcularDireccion(double alpha, double desplazamiento) {
		Coords next = new Coords(getX(), getY());
		next.addX(Math.cos(alpha) * desplazamiento);
		next.addY(Math.sin(alpha) * desplazamiento);
		return next;
	}

	public Coords diferencia(Coords otra) {
		return new Coords(Math.abs(otra.getX() - getX()), Math.abs(otra.getY() - getY()));
	}
	
	public double distancia(Coords otra) {
		return Math.hypot(Math.abs(otra.getX() - getX()), Math.abs(otra.getY() - getY()));
	}

	public double calcularAngulo(Coords destino) {
		return Math.atan2((destino.getY() - getY()), (destino.getX() - getX()));
	}

}
