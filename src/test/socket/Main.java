//package test.socket;
//
//import java.io.IOException;
//import java.net.Socket;
//import java.util.HashMap;
//import java.util.Map;
//
//import com.escenas.Aplicacion;
//import com.escenas.Boton;
//import com.escenas.Campo;
//import com.login.InvalidNameException;
//import com.login.Usuario;
//import com.socket.Cliente;
//import com.socket.EventoSocket;
//import com.socket.Servidor;
//
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.scene.Group;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.control.TextArea;
//import javafx.scene.control.TextField;
//import javafx.scene.layout.VBox;
//import javafx.scene.web.WebView;
//import javafx.stage.Stage;
//import javafx.stage.WindowEvent;
//
//public class Main extends Aplicacion {
//	Servidor server;
//	TextArea print;
//	Map<Socket, Usuario> usuarios;
//
//	public static void main(String args[]) {
//		launch(args);
//	}
//
//	@Override
//	public void start(Stage primaryStage) {
//		usuarios = new HashMap<Socket, Usuario>();
//
//		Group root = new Group();
//		primaryStage.setScene(new Scene(root));
//		print = new TextArea("Esperando conexion...\n");
//		print.setEditable(false);
//		root.getChildren().add(print);
//
//		agregarBotonCliente(root);
//
//		primaryStage.show();
//		try {
//			server = new Servidor(8080);
//			server.waitConection(new EventoSocket() {
//
//				@Override
//				public void run(Object objeto, Socket origen) {
//					if (objeto.getClass().equals(Usuario.class)) {
//						usuarios.put(origen, (Usuario) objeto);
//						mensaje(((Usuario) objeto).getName() + " inicio sesion\n");
//					} else {
//						Usuario user = usuarios.get(origen);
//						if (user != null) {
//							mensaje(user.getName()+":"+objeto + "\n");
//							
//						}
//					}
//				}
//
//			});
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		
//	}
//	
//	
//
//	protected void mensaje(String string) {
//
//		print.appendText(string);
//		try {
//			server.sendObject(string);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//
//	private void agregarBotonCliente(Group root) {
//		Boton launchCliente = new Boton("Cliente", 320d, 290d, root);
//
//		
//		
//		
//		
//		TextField entradaIP = new TextField("localhost");
//		root.getChildren().add(entradaIP);
//		entradaIP.relocate(0d, 300d);
//
//		TextField entradaUser = new TextField("@NoName");
//		root.getChildren().add(entradaUser);
//		entradaUser.relocate(150d, 300d);
//
//		launchCliente.setOnAction(new EventHandler<ActionEvent>() {
//
//			@Override
//			public void handle(ActionEvent arg0) {
//				Stage vCliente = new Stage();
//				VBox root = new VBox();
//				vCliente.setScene(new Scene(root));
//
//				TextArea print = new TextArea();
//				print.setEditable(false);
//				root.getChildren().add(print);
//				vCliente.setX(0);
//
//				TextField sending = new TextField();
//				root.getChildren().add(sending);
//
//				Button bEnviar = new Button("Enviar");
//				root.getChildren().add(bEnviar);
//
//				try {
//
//					Cliente conexion;
//					conexion = new Cliente(entradaIP.getText(), 8080);
//					try {
//						conexion.sendObject(new Usuario(entradaUser.getText()));
//					} catch (InvalidNameException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//
//					bEnviar.setOnAction(new EventHandler<ActionEvent>() {
//
//						@Override
//						public void handle(ActionEvent arg0) {
//							conexion.sendObject(sending.getText());
//							sending.clear();
//
//						}
//					});
//
//					vCliente.setOnCloseRequest(new EventHandler<WindowEvent>() {
//
//						@Override
//						public void handle(WindowEvent arg0) {
//							try {
//								conexion.close();
//							} catch (IOException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//					});
//					conexion.waitObject(new EventoSocket() {
//
//						@Override
//						public void run(Object objeto, Socket origen) {
//							print.appendText(objeto.toString());
//
//						}
//					});
//
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				vCliente.show();
//
//			}
//		});
//
//	}
//
//}
