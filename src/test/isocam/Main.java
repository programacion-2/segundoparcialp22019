package test.isocam;

import com.elementos.Personaje;
import com.escenas.Escena;
import com.tool.Coords;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {

		launch(args);
	}

	protected long then;
	private Personaje aldeano1;

	@Override
	public void start(Stage primaryStage) {

		Group root = new Group();
		Escena escena = new Escena(root);
		primaryStage.setScene(escena);

		Canvas hoja = new Canvas(1000, 1000);
		root.getChildren().add(hoja);

		aldeano1 = new Personaje();
		aldeano1.setPos(100, 400);
//		aldeano1.colocar(hoja);

		GraphicsContext lapiz = hoja.getGraphicsContext2D();

		new AnimationTimer() {

			@Override
			public void handle(long now) {
				// TODO Auto-generated method stub
				long time = now - then;
				if (then == 0) {
					time = 0;
				}
				then = now;

				lapiz.clearRect(0, 0, 1000, 1000);
				if (aldeano1.getDestino() == null)
					aldeano1.setDefaultColor(Color.DARKRED);
				else
					aldeano1.setDefaultColor(Color.DARKGREEN);
				aldeano1.actualizar(((double) time / 1000000000));
				lapiz.fillRect(270, 270, 100, 100);

			}
		}.start();

		escena.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent click) {
				if (click.getButton() == MouseButton.PRIMARY)
					aldeano1.setDestino(new Coords(click.getSceneX(), click.getSceneY()));
				if (click.getButton() == MouseButton.SECONDARY)
					aldeano1.setDestino(null);
			}

		});

		primaryStage.show();
	}

}
