-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-10-2019 a las 14:37:02
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `RPG_Test1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Escenarios`
--

CREATE TABLE `Escenarios` (
  `idEscenario` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Norte` int(11) DEFAULT NULL,
  `Sur` int(11) DEFAULT NULL,
  `Este` int(11) DEFAULT NULL,
  `Oeste` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Escenarios`
--

INSERT INTO `Escenarios` (`idEscenario`, `Nombre`, `Norte`, `Sur`, `Este`, `Oeste`) VALUES
(1, 'Test1', 0, 0, 2, 0),
(2, 'Test2', 0, 0, 0, 1),
(3, 'Test3', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Escenarios_has_Obstaculos`
--

CREATE TABLE `Escenarios_has_Obstaculos` (
  `ID` int(11) NOT NULL,
  `Escenarios_idEscenario` int(11) NOT NULL,
  `Obstaculos_idObstaculo` int(11) NOT NULL,
  `PosX` double DEFAULT NULL,
  `PosY` double DEFAULT NULL,
  `SizeX` double DEFAULT NULL,
  `SizeY` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Escenarios_has_Obstaculos`
--

INSERT INTO `Escenarios_has_Obstaculos` (`ID`, `Escenarios_idEscenario`, `Obstaculos_idObstaculo`, `PosX`, `PosY`, `SizeX`, `SizeY`) VALUES
(1, 1, 1, 771, 235, 300, 300),
(2, 1, 2, 1038, 466, 100, 100),
(3, 1, 2, 147, 685, 100, 100),
(4, 1, 2, 400, 154, 100, 100),
(5, 1, 2, 449, 440, 100, 100),
(6, 2, 1, 1016, 264, 100, 100),
(7, 3, 2, 959, 285, 100, 100),
(8, 2, 2, 1436, 234, 100, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Mundo_has_Escenarios`
--

CREATE TABLE `Mundo_has_Escenarios` (
  `id` int(11) NOT NULL,
  `Escenarios_idEscenario` int(11) NOT NULL,
  `PosX` int(11) NOT NULL,
  `PosY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Mundo_has_Escenarios`
--

INSERT INTO `Mundo_has_Escenarios` (`id`, `Escenarios_idEscenario`, `PosX`, `PosY`) VALUES
(0, 1, -1, 0),
(0, 1, 0, 0),
(0, 1, 1, -1),
(0, 2, 0, 1),
(0, 2, 1, 0),
(0, 3, 0, -1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Obstaculos`
--

CREATE TABLE `Obstaculos` (
  `idObstaculo` int(11) NOT NULL,
  `Imagen` varchar(45) DEFAULT NULL,
  `SizeX` double DEFAULT NULL,
  `SizeY` double DEFAULT NULL,
  `Nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Obstaculos`
--

INSERT INTO `Obstaculos` (`idObstaculo`, `Imagen`, `SizeX`, `SizeY`, `Nombre`) VALUES
(1, 'file:recursos/imagenes/Arbol.png', 100, 100, 'Arbol'),
(2, 'file:recursos/imagenes/Piedra.png', 80, 80, 'Piedra');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `name`, `pass`) VALUES
(1, '@qwerty', '1234'),
(2, '@Cristian', '1234'),
(3, '@Rey', '1234'),
(4, '@otroUsuarioMÃ¡s', '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Escenarios`
--
ALTER TABLE `Escenarios`
  ADD PRIMARY KEY (`idEscenario`),
  ADD KEY `fk_Escenarios_Escenarios1_idx` (`Norte`),
  ADD KEY `fk_Escenarios_Escenarios2_idx` (`Sur`),
  ADD KEY `fk_Escenarios_Escenarios3_idx` (`Este`),
  ADD KEY `fk_Escenarios_Escenarios4_idx` (`Oeste`);

--
-- Indices de la tabla `Escenarios_has_Obstaculos`
--
ALTER TABLE `Escenarios_has_Obstaculos`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Escenarios_has_Obstaculos_Obstaculos1_idx` (`Obstaculos_idObstaculo`),
  ADD KEY `fk_Escenarios_has_Obstaculos_Escenarios_idx` (`Escenarios_idEscenario`);

--
-- Indices de la tabla `Mundo_has_Escenarios`
--
ALTER TABLE `Mundo_has_Escenarios`
  ADD PRIMARY KEY (`id`,`Escenarios_idEscenario`,`PosX`,`PosY`),
  ADD KEY `fk_Mundo_has_Escenarios_Escenarios1_idx` (`Escenarios_idEscenario`);

--
-- Indices de la tabla `Obstaculos`
--
ALTER TABLE `Obstaculos`
  ADD PRIMARY KEY (`idObstaculo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Escenarios`
--
ALTER TABLE `Escenarios`
  MODIFY `idEscenario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Escenarios_has_Obstaculos`
--
ALTER TABLE `Escenarios_has_Obstaculos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `Obstaculos`
--
ALTER TABLE `Obstaculos`
  MODIFY `idObstaculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Escenarios`
--
ALTER TABLE `Escenarios`
  ADD CONSTRAINT `fk_Escenarios_Escenarios1` FOREIGN KEY (`Norte`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Escenarios_Escenarios2` FOREIGN KEY (`Sur`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Escenarios_Escenarios3` FOREIGN KEY (`Este`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Escenarios_Escenarios4` FOREIGN KEY (`Oeste`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Escenarios_has_Obstaculos`
--
ALTER TABLE `Escenarios_has_Obstaculos`
  ADD CONSTRAINT `fk_Escenarios_has_Obstaculos_Escenarios` FOREIGN KEY (`Escenarios_idEscenario`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Escenarios_has_Obstaculos_Obstaculos1` FOREIGN KEY (`Obstaculos_idObstaculo`) REFERENCES `Obstaculos` (`idObstaculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Mundo_has_Escenarios`
--
ALTER TABLE `Mundo_has_Escenarios`
  ADD CONSTRAINT `fk_Mundo_has_Escenarios_Escenarios1` FOREIGN KEY (`Escenarios_idEscenario`) REFERENCES `Escenarios` (`idEscenario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
