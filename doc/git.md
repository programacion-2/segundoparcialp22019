# Git
Git es un "sistema de control de versiones" usado por muchos programadores - es un sistema que registra los cambios en
los archivos a través del tiempo de forma tal que puedas acceder a versiones específicas cuando lo desees. Es muy
similar a la opción de ***"registrar cambios"*** en Microsoft Word, pero mucho más poderoso.

¿Qué es control de versiones? Pues bien, se define como control de versiones a la gestión de los diversos cambios que se realizan sobre los elementos de algún producto o una configuración del mismo es decir a la gestión de los diversos cambios que se realizan sobre los elementos de algún producto o una configuración, y para los que aún no les queda claro del todo, control de versiones es lo que se hace al momento de estar desarrollando un software o una página web. Exactamente es eso que haces cuando subes y actualizas tu código en la nube, o le añades alguna parte o simplemente le editas cosas que no funcionan como deberían o al menos no como tú esperarías.

Y, entonces ¿a que le llamamos sistema de control de versiones? Muy sencillo, son todas las herramientas que nos permiten hacer todas esas modificaciones antes mencionadas en nuestro código y hacen que sea más fácil la administración de las distintas versiones de cada producto desarrollado.



## Crear nuestro propio repositorio Git
Git rastrea los cambios realizados a un grupo determinado de ficheros en lo que llamamos un repositorio de código (o
"repo" para abreviar). Iniciemos uno para nuestro proyecto. Abre la consola y ejecuta los siguientes comandos en el
directorio elegido :

> Nota: Comprueba el directorio de trabajo actual con el comando  pwd (OSX/Linux) o cd (Windows) antes de
> inicializar el repositorio. Deberías estar en la carpeta "test git" .

~~~
$ git init
Initialized empty Git repository in  C:/Users/nombre/Google Drive/CESIT/PROGRAMACION 2/test git/.git/
$ git config user.name "Tu nombre"
$ git config user.email tú@ejemplo.com
~~~

Inicializar el repositorio git es algo que sólo necesitamos hacer una vez por proyecto. Git llevará un registro de los cambios realizados en todos los ficheros y carpetas en este directorio, pero hay algunos ficheros que queremos que ignore. Esto lo hacemos creando un fichero llamado .gitignore en el directorio base.  
Por ejemplo Creamos el siguiente archivo:

~~~
.gitignore
*.class
comentario.txt
target/
bin/
build/
~~~

Y guárdalo como **` .gitignore `** en la primera carpeta.

Podemos usar el comando ` git status `

~~~
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        test.java

nothing added to commit but untracked files present (use "git add" to track)
~~~

Y finalmente guardamos nuestros cambios. Ve a la consola y ejecuta estos comandos:

~~~
$ git add --all .
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   test.java
~~~

Al terminar creamos un ***"commit"*** con un mensaje para encontrarlo entre las diferentes versiones de nuestro proyecto

~~~
$ git commit -m "Test de Git"
~~~
## Subir proyecto gitlab (simiar github)
Visita Gitlab.com y registra una nueva cuenta de usuario gratuita. Luego, crea un nuevo proyecto (New Project) con el nombre "Testprogramacion2". En Project name agregamos el nombre del proyecto, puedes agregarle una descripcion en Project description esto es opcional. Le hacemos click al Visibility Level (Nivel de visibilidad), si lo queres en Private (privado) o Public (publico). Deja desmarcada la opción "Initialise with a README" y le damos click a cear proyecto. Te va aparecer Project 'Tutorial' was successfully created (El proyecto ha sido creado con exito).

Una vez creado el proyecto vamos a Settings(configuraciones) /members(miembros). En GitLab member or Email address agregamos el email de los integrantes. Podemos seleccionar el permiso que le damos a los integrantes en Choose a role permission: 
- Guest (Huesped): Tiene acceso muy limitado
- Reporter (Reportero): Puede leer y copiar
- Developer (Desarrollador): Puede leer y subir en branch secundarios
- Maintainer (Mantenedor): Puede leer y editar en branch master

Y le damos Add to project (Agregar al proyecto)

En la próxima pantalla verás la URL para clonar tu repositorio. Elige la versión "HTTPS", cópiala y en un momento la
pegaremos en la consola:

https://gitlab.com/minombre/Testprogramacion2.git

Ahora tenemos que conectar el repositorio Git de tu ordenador con el que está en GitLab.

~~~
$ git remote add origin https://gitlab.com/minombre/Testprogramacion2.git
$ git push -u origin master
~~~

**Escribe tu nombre de usuario y contraseña de GitLab y deberías ver algo así:**

~~~
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 220 bytes | 110.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://github.com/minombre/Testprogramacion2.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
~~~
## Descargar proyecto
***para descargar nuestro repositorio en otra pc usamos***

~~~
$ git clone https://github.com/minombre/Testprogramacion2.git
~~~



***para actualizar el repositor en la pc master:***

~~~bash

cd testprogramacion2

# Creamos o modificamos el archivo test

$  git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   test.java

no changes added to commit (use "git add" and/or "git commit -a")

$ git add --all

$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   test.java


$ git commit -m "Primera actualizacion, HOLAMUNDO"
[master 179946b] Primera actualizacion, HOLAMUNDO
 1 file changed, 8 insertions(+)

$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 367 bytes | 183.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://github.com/minombre/Testprogramacion2.git
   bf091fb..179946b  master -> master

~~~

**para actualizar el repositor en la segunda pc:**

~~~
$ cd Testprogramacion2

$ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 3 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
From https://github.com/minombre/Testprogramacion2
   bf091fb..179946b  master     -> origin/master
Updating bf091fb..179946b
Fast-forward
 test.java | 8 ++++++++
 1 file changed, 8 insertions(+)
~~~

## Otros comandos utiles 

A la hora de agregar ***"git add ."***


podemos borrar algun archivo agregado por error con 

~~~
$ git rm --cached test.java
~~~

o podemos quitar algun "cambio" agregado por error sin sacar al archivo del repositorio

~~~

git reset HEAD test.java
~~~

incluso podemos deshacer los cambios de algun archivo, regresandolo o a la version del repositorio

~~~
 git checkout  test.java
~~~
