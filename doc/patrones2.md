## ¿que es un patrón de diseño?
Los patrones de diseño son unas técnicas para resolver problemas comunes en el desarrollo de software y otros ámbitos referentes al diseño de interacción o interfaces.

Un patrón de diseño resulta ser una solución a un problema de diseño. Para que una solución sea considerada un patrón debe poseer ciertas características. Una de ellas es que debe haber comprobado su efectividad resolviendo problemas similares en ocasiones anteriores.

# tipos de patrones

## patrón singleton

es un patrón de diseño que permite restringir la creación de objetos pertenecientes a una clase o el valor de un tipo a un único objeto.

![diagrama de clase del singleton](https://www.ediciones-eni.com/Open/download/c0dbc5b4-e40d-4c7f-a7c5-1a32abbb364f/images/fig811.PNG)

El constructor tiene visibilidad privada por lo que no sera posible crear objetos directamente desde un new.
Para crearlo se usa el método Instance() que primero verifica que no se haya creado ningúna otra instancia.
Al crear la instancia esta se guarda en un atributo privado y será devuelto cada vez q se llame a Instance().

Ejemplo:
~~~Java
import java.util.*;  
public class DocumentacionEnBlanco extends Documentacion  
{  
    private static DocumentacionEnBlanco _instance = null;  
  
    private DocumentacionEnBlanco()  
    {  
        documentos = new ArrayList<Documento>();  
    }  
  
    public static DocumentacionEnBlanco Instance()  
    {  
        if (_instance == null)  
            _instance = new DocumentacionEnBlanco();  
        return _instance;  
    }  
  
     ...  
}
~~~



## patrón factory 

consiste en utilizar una clase constructora abstracta con unos cuantos métodos definidos y otro(s) abstracto(s): el dedicado a la construcción de objetos de un subtipo de un tipo determinado. Es una simplificación del Abstracto Factory, en la que la clase abstracta tiene métodos concretos que usan algunos de los abstractos; según usemos una u otra hija de esta clase abstracta, tendremos uno u otro comportamiento.

Ejemplo:

Se creará una clase FabricaVehiculo que permitirá crear objetos Automovil o Scooter, estos pueden ser eléctricos o a gasolina. para ello se crea las clases AutomovilElectricidad y AutomovilGasolina que heredan de la clase abstracta Automovil y ScooterElectricidad y ScooterGasolina que heredan de la abstracta Scooter. Luego la FabricaVehiculo que posee los métodos creaAutomovil y creaScooter la heredamos en FabricaVehiculoGasolina y FabricaVehiculoElectricidad. Como resultados tenemos la posibilidad de usar el método creaAutomovil() desde un objeto FabricaVehiculoGasolina para crear un AutomovilGasolina.

Por parte: primero creamos las clases Automovil

~~~Java
public abstract class Automovil { 
    protected String modelo;
    protected String color;
    protected int potencia;
    protected double espacio;
    public Automovil(String modelo, String color, int potencia, double espacio) {
        this.modelo = modelo;
        this.color = color;
        this.potencia = potencia;
        this.espacio = espacio;
    }

    public abstract void mostrarCaracteristicas();
}
public class AutomovilElectricidad extends Automovil {
    public AutomovilElectricidad(String modelo, String color, int potencia, double espacio) {
        super(modelo, color, potencia, espacio);
    }
    public void mostrarCaracteristicas(){
        System.out.println( "Automovil electrico de modelo: " + modelo +
        " de color: " + color + " de potencia: " + potencia + " de espacio: " + espacio);
    }
}
public class AutomovilGasolina extends Automovil {
    public AutomovilGasolina(String modelo, String color, int potencia, double espacio) {
        super(modelo, color, potencia, espacio);
    }
    public void mostrarCaracteristicas() {
        System.out.println( "Automovil de gasolina de modelo: " + modelo +
        " de color: " + color + " de potencia: " + potencia + " de espacio: " + espacio);
    }
}
~~~

 después creamos las clases Scooter

~~~Java
public abstract class Scooter {
	protected String modelo;
	protected String color;
	protected int potencia;

	public Scooter(String modelo, String color, int potencia) {
		this.modelo = modelo;
		this.color = color;
		this.potencia = potencia;
	}

	public abstract void mostrarCaracteristicas();
}

public class ScooterElectricidad extends Scooter {
	public ScooterElectricidad(String modelo, String color, int potencia) {
		super(modelo, color, potencia);
	}

	public void mostrarCaracteristicas() {
		System.out.println(
				"Scooter electrica de modelo: " + modelo + " de color: " + color + " de potencia: " + potencia);
	}
}

public class ScooterGasolina extends Scooter {
	public ScooterGasolina(String modelo, String color, int potencia) {
		super(modelo, color, potencia);
	}

	public void mostrarCaracteristicas() {
		System.out.println(
				"Scooter de gasolina de modelo: " + modelo + " de color: " + color + " de potencia: " + potencia);
	}
}
~~~

Y las clases FabricaVehiculo

~~~Java
public interface FabricaVehiculo {
	Automovil creaAutomovil(String modelo, String color, int potencia, double espacio);

	Scooter creaScooter(String modelo, String color, int potencia);
}

public class FabricaVehiculoElectricidad implements FabricaVehiculo {
	public Automovil creaAutomovil(String modelo, String color, int potencia, double espacio) {
		return new AutomovilElectricidad(modelo, color, potencia, espacio);
	}

	public Scooter creaScooter(String modelo, String color, int potencia) {
		return new ScooterElectricidad(modelo, color, potencia);
	}
}

public class FabricaVehiculoGasolina implements FabricaVehiculo {
	public Automovil creaAutomovil(String modelo, String color, int potencia, double espacio) {
		return new AutomovilGasolina(modelo, color, potencia, espacio);
	}

	public Scooter creaScooter(String modelo, String color, int potencia) {
		return new ScooterGasolina(modelo, color, potencia);
	}
}

~~~

En el programa principal usa el atributo "FabricaVehiculo fabrica" para crear un Automovil usando fabrica.creaAutomovil(). Si fabrica contenía un FabricaVehiculoElectricidad el objeto final será un AutomovilElectricidad.


~~~Java
import java.util.*;

public class Catalogo {
	public static int nAutos = 3;
	public static int nScooters = 2;

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		FabricaVehiculo fabrica;
		Automovil[] autos = new Automovil[nAutos];
		Scooter[] scooters = new Scooter[nScooters];
		System.out.print("Desea utilizar " + "vehiculos electricos (1) o a gasolina (2):");
		String eleccion = reader.next();
		if (eleccion.equals("1")) {
			fabrica = new FabricaVehiculoElectricidad();
		} else {
			fabrica = new FabricaVehiculoGasolina();
		}
		for (int index = 0; index < nAutos; index++)
			autos[index] = fabrica.creaAutomovil("estandar", "amarillo", 6 + index, 3.2);
		for (int index = 0; index < nScooters; index++)
			scooters[index] = fabrica.creaScooter("clasico", "rojo", 2 + index);
		for (Automovil auto : autos)
			auto.mostrarCaracteristicas();
		for (Scooter scooter : scooters)
			scooter.mostrarCaracteristicas();
	}
}
~~~

## PATRÓN Facade

es un tipo de patrón de diseño estructural. Viene motivado por la necesidad de estructurar un entorno de programación y reducir su complejidad con la división en subsistemas, minimizando las comunicaciones y dependencias entre estos.

El patrón Facade encapsula la interfaz de cada objeto considerada como interfaz de bajo nivel en una interfaz única de nivel más elevado. La construcción de la interfaz unificada puede necesitar implementar métodos destinados a componer las interfaces de bajo nivel.

Ejemplo:

WebServiceAutoImpl es una implementación de la interfaz webServiceAuto, la que permite buscar vehículos dentro de un rango de precios y acceder a documentos en base al indice. Para hacer esto estará compuesto de Catalogo y de GestionDocumento agrupando estas 2 interfaces en una sola.

El programa principal podría ser el siguiente:

~~~Java
import java.util.*;
public class UsuarioWebService{
    public static void main(String[] args){
        WebServiceAuto webServiceAuto = new WebServiceAutoImpl();
        System.out.println(webServiceAuto.documento(0));
        System.out.println(webServiceAuto.documento(1));
        List<String> resultados = webServiceAuto.buscaVehiculos(6000, 1000);
        if (resultados.size() > 0){
            System.out.println("Vehículo(s) cuyo precio está comprendido "+
            "entre 5000 y 7000");
            for (String resultado: resultados)
            System.out.println("    " + resultado);
        }
    }
}
~~~

La interfaz WebServiceAuto deberá tener los métodos documento y buscaVehiculos

~~~Java

import java.util.List;
public interface WebServiceAuto{
    String documento(int indice);
    List<String> buscaVehiculos(int precioMedio, int desviacionMax);
}
~~~

Para implementarlo hace uso de los componentes mencionados anteriormente.

~~~Java
import java.util.List;
public class WebServiceAutoImpl implements WebServiceAuto{
    protected Catalogo catalogo = new ComponenteCatalogo();
    protected GestionDocumento gestionDocumento = new ComponenteGestionDocumento();

    public String documento(int indice){
        return gestionDocumento.documento(indice);
    }
    
    public List<String> buscaVehiculos(int precioMedio,int desviacionMax){
        return catalogo.buscaVehiculos(precioMedio -desviacionMax, precioMedio + desviacionMax);
    }
}
~~~

El ComponenteGestionDocumento es una implementación de GestionDocumento.

~~~Java
public interface GestionDocumento {
    String documento(int indice);
    }

public class ComponenteGestionDocumento implements GestionDocumento {
    
    public String documento(int indice){
        return "Documento número " + indice;
    }
}
~~~

El ComponenteCatalogo es una implementación de Catalogo.

~~~Java
import java.util.*;
public interface Catalogo {
    List<String> buscaVehiculos(int precioMin, int precioMax);
    }
    

public class ComponenteCatalogo implements Catalogo { 
    protected Object[] descripcionVehiculo = {
        "Berlina 5 puertas", 6000, "Compacto 3 puertas", 4000,
        "Espace 5 puertas", 8000, "Break 5 puertas", 7000,
        "Coupé 2 puertas", 9000, "Utilitario 3 puertas", 5000
    };
    //nota del editor: que clase de programador mete en el mismo arreglo descripciones y precios? 

    public List<String> buscaVehiculos(int precioMin, int precioMax) {
        int indice, tamaño;
        //nota del editor: Claro, el mismo tipo de programador que usa ñ en sus variables
        List<String> resultado = new ArrayList<String>();
        tamaño = descripcionVehiculo.length / 2;
        for (indice = 0; indice < tamaño; indice++){
            int precio = (Integer)descripcionVehiculo[2 * indice + 1];
            if ((precio >= precioMin) && (precio <= precioMax))
                resultado.add((String)descripcionVehiculo[2 * indice]);
        }
        return resultado;
    }
}
~~~



## PATRON Builder 

 el patrón builder (Constructor) es usado para permitir la creación de una variedad de objetos complejos desde un objeto fuente (Producto), el objeto fuente se compone de una variedad de partes que contribuyen individualmente a la creación de cada objeto complejo a través de un conjunto de llamadas a interfaces comunes de la clase Abstracto Builder.

Ejemplo:

~~~java
import java.util.*;

public abstract class Documentacion{
    protected List<String>contenido =
        new ArrayList<String>();
    
    public abstract void agregaDocumento(String documento);
    
    public abstract void imprime();
}

public class DocumentacionHtml extends Documentacion{
    public void agregaDocumento(String documento){
        if (documento.startsWith("<HTML>"))
            contenido.add(documento);
    }

    public void imprime(){
        System.out.println("Documentacion HTML");
        for (String s: contenido)
            System.out.println(s);
    }
}

public class DocumentacionPdf extends Documentacion {
     public void agregaDocumento(String documento){
          if (documento.startsWith("<PDF>"))               
            contenido.add(documento);
     }
      public void imprime(){
          System.out.println("Documentacion PDF");
          for (String s: contenido)
            System.out.println(s);
        }   
}

~~~

El código fuente de las clases que generan la documentación aparece a continuación

~~~java

public abstract class ConstructorDocumentacionVehiculo{
     protected Documentacion documentacion;     
     
     public abstract void construyeSolicitudPedido(String nombreCliente);
     public abstract void construyeSolicitudMatriculacion(String nombreSolicitante);
     public Documentacion resultado(){
         return documentacion; 
     }
}

public class ConstructorDocumentacionVehiculoHtml extends  ConstructorDocumentacionVehiculo{
     public ConstructorDocumentacionVehiculoHtml(){
         documentacion = new DocumentacionHtml();
    }
    public void construyeSolicitudPedido(String nombreCliente){
        String documento;
        documento = "<HTML>Solicitud de pedido Cliente: " + nombreCliente + "</HTML>";
        documentacion.agregaDocumento(documento);
    }
    public void construyeSolicitudMatriculacion(String nombreSolicitante){
        String documento;
        documento = "<HTML>Solicitud de matriculacion Solicitante: " + nombreSolicitante + "</HTML>";
        documentacion.agregaDocumento(documento);
    }   
}

public class ConstructorDocumentacionVehiculoPdf extends ConstructorDocumentacionVehiculo{
    public ConstructorDocumentacionVehiculoPdf(){
        documentacion = new DocumentacionPdf(); 
    }
    public void construyeSolicitudPedido(String nombreCliente){
        String documento;
        documento = "<PDF>Solicitud de pedido Cliente: " + nombreCliente + "</PDF>"; 
        documentacion.agregaDocumento(documento);
    }
    public void construyeSolicitudMatriculacion (String nombreSolicitante){
        String documento;
        documento = "<PDF>Solicitud de matriculacion Solicitante: " + nombreSolicitante + "</PDF>";
        documentacion.agregaDocumento(documento);
    }
}

~~~

La clase Vendedor se describe a continuación. Su constructor recibe como parámetro una instancia de ConstructorDocumentacionVehiculo. Observe que el método construye toma como parámetro la información del cliente, aquí limitada al nombre del cliente. 

~~~java

public class Vendedor{
    protected ConstructorDocumentacionVehiculo constructor;   
    
    public Vendedor(ConstructorDocumentacionVehiculo constructor){
        this.constructor = constructor;
    }
    public Documentacion construye(String nombreCliente){
        constructor.construyeSolicitudPedido(nombreCliente);
        constructor.construyeSolicitudMatriculacion(nombreCliente);
        Documentacion documentacion = constructor.resultado();
        return documentacion;
        }
    }

~~~
Por último, se proporciona el código Java del cliente del constructor, a saber la clase ClienteVehiculo que constituye el programa principal. El inicio de este programa solicita al usuario el constructor que debe utilizar, y se lo proporciona a continuación al vendedor.

~~~java

import java.util.*;
public class ClienteVehiculo{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);       
        ConstructorDocumentacionVehiculo constructor;        
        System.out.print("Desea generar " + "documentacion HTML (1) o PDF (2):");
        String seleccion = reader.next();
        if (seleccion.equals("1")){
            constructor = new ConstructorDocumentacionVehiculoHtml();
        }
        else{
            constructor = new ConstructorDocumentacionVehiculoPdf();
        }
        Vendedor vendedor = new Vendedor(constructor);  
        Documentacion documentacion = vendedor.construye("Martin");
        documentacion.imprime();
    }
} 

~~~

Un ejemplo de ejecución para una documentación PDF sería:
Desea generar documentacion HTML (1) o PDF (2):2 
Documentacion PDF
<PDF>Solicitud de pedido Cliente: Martin</PDF>
<PDF>Solicitud de matriculacion Solicitante: Martin</PDF>
Conforme a la solicitud del cliente, la documentación y sus documentos se han creado en formato PDF. Si el cliente solicitara su documentación en HTML, la salida sería la siguiente:
Desea generar documentacion HTML (1) o PDF (2):1
Documentacion HTML
<HTML>Solicitud de pedido Cliente: Martin</HTML>
<HTML>Solicitud de matriculacion Solicitante: Martin</HTML> 
~~~



## patrón STRATEGY

Se clasifica como patrón de comportamiento porque determina cómo se debe realizar el intercambio de mensajes entre diferentes objetos para resolver una tarea.

El patrón Strategy tiene como objetivo adaptar el comportamiento y los algoritmos de un objeto en función de una necesidad sin cambiar las interacciones de este objeto con los clientes.
Esta necesidad puede ponerse de relieve en base a aspectos tales como la presentación, la eficacia en tiempo de ejecución o en memoria, la elección de algoritmos, la representación interna, etc. Aunque evidentemente no se trata de una necesidad funcional de cara a los clientes del objeto, pues las interacciones entre el objeto y sus clientes deben permanecer inmutables.


Ejemplo: 
La siguiente interfaz se usa para graficar una lista de vehiculos. Usamos 2 implementaciones para dibujarlas individualmente o de a 3.

~~~Java
import java.util.*;
public interface DibujaCatalogo {
    void dibuja(List<VistaVehiculo> contenido);
}

//Esta clase "dibuja" un vehiculo por linea
public class DibujaUnVehiculoPorLinea implements DibujaCatalogo {
    public void dibuja(List<VistaVehiculo> contenido) {
        System.out.println( "Dibuja los vehiculos mostrando un vehiculo por linea");
        for (VistaVehiculo vistaVehiculo: contenido) {
            vistaVehiculo.dibuja(); 
            System.out.println();
        }
        System.out.println();
    }
}

//Esta clase "dibuja" tres  vehiculos por linea
public class DibujaTresVehiculosPorLinea implements DibujaCatalogo { 
    public void dibuja(List<VistaVehiculo> contenido) {
        int contador;System.out.println("Dibuja los vehículos mostrando tres vehículos por linea");
        contador = 0;
        for (VistaVehiculo vistaVehiculo: contenido) {
            vistaVehiculo.dibuja();
            contador++;
            if (contador == 3) {
                System.out.println();
                contador = 0;
            } else
            System.out.println(" ");
        }
        if (contador != 0)
            System.out.println();
        System.out.println();}
    }
}
~~~

La clase VistaVehiculo almacena el "dibujo" de cada vehículo

~~~Java
public class VistaVehiculo { 
    protected String descripcion;
    public VistaVehiculo(String descripcion) {
        this.descripcion = descripcion;
    }
    public void dibuja() {
        System.out.print(descripcion);
    }
}
~~~

VistaCatalogo almacena los diferentes VistaVehiculo y utiliza alguna implementacion de DibujaCatalogo para mostrarlos.

~~~Java
import java.util.*;
public class VistaCatalogo {
    protected List<VistaVehiculo> contenido = new ArrayList<VistaVehiculo>();
    protected DibujaCatalogo dibujo;
    public VistaCatalogo(DibujaCatalogo dibujo) {
        contenido.add(new VistaVehiculo("vehículo económico"));
        contenido.add(new VistaVehiculo("vehículo amplio"));
        contenido.add(new VistaVehiculo("vehículo rápido"));
        contenido.add(new VistaVehiculo("vehículo confortable"));
        contenido.add(new VistaVehiculo("vehículo deportivo"));
        this.dibujo = dibujo;
    }
}
~~~

Si a un objeto de VistaCatalogo se le carga un DibujaTresVehiculosPorLinea al dibujarlos lo hará de 3 en 3 y si se carga un DibujaUnVehiculoPorLinea lo hará por separado. Esto totalmenteindependiente del codigo de VistaCatalogo, la forma de mostrarlo esta determinado por el DibujaCatalogo que se le envíe.

~~~Java
public class Usuario { 
    public static void main(String[] args) {
        VistaCatalogo vistaCatalogo1 = new VistaCatalogo(new DibujaTresVehiculosPorLinea());
        vistaCatalogo1.dibuja();
        VistaCatalogo vistaCatalogo2 = new VistaCatalogo(new DibujaUnVehiculoPorLinea());
        vistaCatalogo2.dibuja();
    }
}
~~~

## ¿que son los antipatrones?

Un antipatrón de diseño es un patrón de diseño que invariablemente conduce a una mala solución para un problema.

Al documentarse los antipatrones, además de los patrones de diseño, se dan argumentos a los diseñadores de sistemas para no escoger malos caminos, partiendo de documentación disponible en lugar de simplemente la intuición.
